iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 677 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 675 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.var_access_expr {
                  } attributes {id = 676 : i64, isExternal = true, name = "var_runtime"}
                } attributes {id = 674 : i64, op = 0 : i64}
              } attributes {id = 678 : i64, op = 0 : i64}
            } attributes {id = 673 : i64}
          } attributes {id = 672 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 253 : i64}
    } attributes {caches = {}, id = 671 : i64, loopOrder = 0 : i64}
  } attributes {id = 250 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_250"}
    } attributes {id = 251 : i64}
  } attributes {id = 250 : i64}
} {
  iir.if {
    iir.expr_statement {
      iir.binary_operator {
        iir.var_access_expr {
        } attributes {id = 235 : i64, isExternal = true, name = "var_runtime"}
      } {
        iir.literal_access_expr {builtinType = 3 : i64, id = 236 : i64, value = "1"}
      } attributes {id = 234 : i64, op = 8 : i64}
    } attributes {id = 237 : i64}
  } {
    iir.block_statement {
      iir.stencil_call_decl_statement {
        iir.stencil_call {arguments = [], callee = "__code_gen_250"}
      } attributes {id = 251 : i64}
    } attributes {id = 246 : i64}
  } {
  } attributes {id = 247 : i64}
} attributes {accessIDToName = {_228 = "var_compiletime", _229 = "var_runtime", _230 = "in", _231 = "out"}, accessIDType = {_228 = 0 : i64, _229 = 0 : i64, _230 = 6 : i64, _231 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [230, 231], fieldAccessIDs = [230, 231], fieldIDToLegalDimensions = {_230 = [1, 1, 1], _231 = [1, 1, 1]}, filename = "../../stencil_desc_ast.cpp", globalVariableIDs = [228, 229], globalVariableToValue = {var_compiletime = {type = 1 : i64, valueIsSet = false}, var_runtime = {type = 1 : i64, value = 1 : i64, valueIsSet = true}}, literalIDToName = {__249 = "1"}, stencilName = "test_01_stencil", temporaryFieldIDs = [], variableVersions = {}}
