iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 199 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 198 : i64, value = "2.2000000000000002"}
              } attributes {id = 200 : i64, op = 0 : i64}
            } attributes {id = 197 : i64}
          } attributes {id = 196 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<1048576, -2, 1048576, 0>}
      } attributes {id = 106 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 204 : i64, name = "tmp", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 203 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 205 : i64, op = 0 : i64}
            } attributes {id = 202 : i64}
          } attributes {id = 201 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 0, 0>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 211 : i64, name = "tmp", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 209 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 210 : i64, value = "2"}
                } attributes {id = 208 : i64, op = 2 : i64}
              } attributes {id = 212 : i64, op = 0 : i64}
            } attributes {id = 207 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 215 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 214 : i64, name = "tmp", negateOffset = false, offset = [0, 0, -1]}
              } attributes {id = 216 : i64, op = 0 : i64}
            } attributes {id = 213 : i64}
          } attributes {id = 206 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 1, 1048576, 0>}
      } attributes {id = 99 : i64}
    } attributes {caches = {_49 = #iir.cache<accessID: 49, type: "K", policy: "Flush", interval: [0, 1, 1048576, 0], enclosingAccessInterval: [0, 1, 1048576, 0], window: [0, 0]>, _50 = #iir.cache<accessID: 50, type: "K", policy: "EPFlush", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0], window: [-3, 0]>}, id = 115 : i64, loopOrder = 1 : i64}
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 220 : i64, name = "tmp", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 219 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 221 : i64, op = 0 : i64}
            } attributes {id = 218 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 224 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 223 : i64, name = "tmp", negateOffset = false, offset = [0, 0, 1]}
              } attributes {id = 225 : i64, op = 0 : i64}
            } attributes {id = 222 : i64}
          } attributes {id = 217 : i64}
        } attributes {id = 4 : i64, interval = #iir.interval<0, 0, 1048576, -4>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 233 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 230 : i64, name = "tmp", negateOffset = false, offset = [0, 0, 3]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 231 : i64, name = "tmp", negateOffset = false, offset = [0, 0, 2]}
                  } attributes {id = 229 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 232 : i64, name = "tmp", negateOffset = false, offset = [0, 0, 1]}
                } attributes {id = 228 : i64, op = 0 : i64}
              } attributes {id = 234 : i64, op = 0 : i64}
            } attributes {id = 227 : i64}
          } attributes {id = 226 : i64}
        } attributes {id = 3 : i64, interval = #iir.interval<1048576, -3, 1048576, -3>}
      } attributes {id = 113 : i64}
    } attributes {caches = {_49 = #iir.cache<accessID: 49, type: "K", policy: "Fill", interval: [0, 0, 1048576, -4], enclosingAccessInterval: [0, 0, 1048576, -4]>, _50 = #iir.cache<accessID: 50, type: "K", policy: "BPFill", interval: [0, 0, 1048576, -3], enclosingAccessInterval: [0, 0, 1048576, 0], window: [0, 3]>}, id = 116 : i64, loopOrder = 2 : i64}
  } attributes {id = 51 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_51"}
    } attributes {id = 52 : i64}
  } attributes {id = 51 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_51"}
  } attributes {id = 52 : i64}
} attributes {accessIDToName = {_47 = "in", _48 = "out", _49 = "b", _50 = "tmp"}, accessIDType = {_47 = 6 : i64, _48 = 6 : i64, _49 = 3 : i64, _50 = 3 : i64}, allocatedFieldIDs = [], apiFieldIDs = [47, 48], fieldAccessIDs = [47, 48, 49, 50], fieldIDToLegalDimensions = {_47 = [1, 1, 1], _48 = [1, 1, 1], _49 = [1, 1, 1], _50 = [1, 1, 1]}, filename = "../../kcache_epflush.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__104 = "2", __108 = "2.2000000000000002"}, stencilName = "kcache_epflush", temporaryFieldIDs = [49, 50], variableVersions = {}}
