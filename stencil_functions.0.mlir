iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 274 : i64, name = "in", negateOffset = false, offset = [1, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 275 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 273 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 272 : i64, name = "__local_delta_268", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 278 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.var_access_expr {
                } attributes {id = 277 : i64, isExternal = false, name = "__local_delta_268"}
              } attributes {id = 279 : i64, op = 0 : i64}
            } attributes {id = 276 : i64}
          } attributes {id = 271 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 138 : i64}
    } attributes {caches = {}, id = 267 : i64, loopOrder = 0 : i64}
  } attributes {id = 126 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_126"}
    } attributes {id = 127 : i64}
  } attributes {id = 126 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_126"}
  } attributes {id = 127 : i64}
} attributes {accessIDToName = {_124 = "in", _125 = "out", _268 = "__local_delta_268"}, accessIDType = {_124 = 6 : i64, _125 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [124, 125], fieldAccessIDs = [124, 125], fieldIDToLegalDimensions = {_124 = [1, 1, 1], _125 = [1, 1, 1]}, filename = "../../stencil_functions.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "test_01_stencil", temporaryFieldIDs = [], variableVersions = {}}
