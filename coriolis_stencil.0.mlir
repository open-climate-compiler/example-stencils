iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 192 : i64, name = "fc", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 194 : i64, name = "v_nnow", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 195 : i64, name = "v_nnow", negateOffset = false, offset = [1, 0, 0]}
                } attributes {id = 193 : i64, op = 0 : i64}
              } attributes {id = 191 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 190 : i64, name = "z_fv_north", op = 0 : i64, type = {builtinType = 4 : i64, is_const = false, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 198 : i64, name = "fc", negateOffset = false, offset = [0, -1, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 200 : i64, name = "v_nnow", negateOffset = false, offset = [0, -1, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 201 : i64, name = "v_nnow", negateOffset = false, offset = [1, -1, 0]}
                } attributes {id = 199 : i64, op = 0 : i64}
              } attributes {id = 197 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 196 : i64, name = "z_fv_south", op = 0 : i64, type = {builtinType = 4 : i64, is_const = false, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 208 : i64, name = "u_tens", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 204 : i64, value = "0.25"}
                } {
                  iir.binary_operator {
                    iir.var_access_expr {
                    } attributes {id = 206 : i64, isExternal = false, name = "z_fv_north"}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 207 : i64, isExternal = false, name = "z_fv_south"}
                  } attributes {id = 205 : i64, op = 0 : i64}
                } attributes {id = 203 : i64, op = 2 : i64}
              } attributes {id = 209 : i64, op = 1 : i64}
            } attributes {id = 202 : i64}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 212 : i64, name = "fc", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 214 : i64, name = "u_nnow", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 215 : i64, name = "u_nnow", negateOffset = false, offset = [0, 1, 0]}
                } attributes {id = 213 : i64, op = 0 : i64}
              } attributes {id = 211 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 210 : i64, name = "z_fu_east", op = 0 : i64, type = {builtinType = 4 : i64, is_const = false, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 218 : i64, name = "fc", negateOffset = false, offset = [-1, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 220 : i64, name = "u_nnow", negateOffset = false, offset = [-1, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 221 : i64, name = "u_nnow", negateOffset = false, offset = [-1, 1, 0]}
                } attributes {id = 219 : i64, op = 0 : i64}
              } attributes {id = 217 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 216 : i64, name = "z_fu_west", op = 0 : i64, type = {builtinType = 4 : i64, is_const = false, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 228 : i64, name = "v_tens", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 224 : i64, value = "0.25"}
                } {
                  iir.binary_operator {
                    iir.var_access_expr {
                    } attributes {id = 226 : i64, isExternal = false, name = "z_fu_east"}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 227 : i64, isExternal = false, name = "z_fu_west"}
                  } attributes {id = 225 : i64, op = 0 : i64}
                } attributes {id = 223 : i64, op = 2 : i64}
              } attributes {id = 229 : i64, op = 2 : i64}
            } attributes {id = 222 : i64}
          } attributes {id = 189 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 96 : i64}
    } attributes {caches = {_45 = #iir.cache<accessID: 45, type: "K", policy: "FillFlush", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>, _47 = #iir.cache<accessID: 47, type: "K", policy: "FillFlush", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>}, id = 104 : i64, loopOrder = 0 : i64}
  } attributes {id = 50 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_50"}
    } attributes {id = 51 : i64}
  } attributes {id = 50 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_50"}
  } attributes {id = 51 : i64}
} attributes {accessIDToName = {_101 = "__local_z_fu_east_101", _102 = "__local_z_fu_west_102", _45 = "u_tens", _46 = "u_nnow", _47 = "v_tens", _48 = "v_nnow", _49 = "fc", _98 = "__local_z_fv_north_98", _99 = "__local_z_fv_south_99"}, accessIDType = {_45 = 6 : i64, _46 = 6 : i64, _47 = 6 : i64, _48 = 6 : i64, _49 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [45, 46, 47, 48, 49], fieldAccessIDs = [45, 46, 47, 48, 49], fieldIDToLegalDimensions = {_45 = [1, 1, 1], _46 = [1, 1, 1], _47 = [1, 1, 1], _48 = [1, 1, 1], _49 = [1, 1, 1]}, filename = "../../coriolis_stencil.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__100 = "0.25", __103 = "0.25"}, stencilName = "coriolis_stencil", temporaryFieldIDs = [], variableVersions = {}}
