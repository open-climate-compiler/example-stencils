iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 521 : i64, name = "acrlat0", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 522 : i64, name = "eddlon", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 520 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 519 : i64, name = "frac_1_dx", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 525 : i64, name = "eddlat", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 526 : i64, value = "6371229"}
              } attributes {id = 524 : i64, op = 3 : i64}
            } attributes {dimension = 0 : i64, id = 523 : i64, name = "frac_1_dy", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 529 : i64, name = "v_in", negateOffset = false, offset = [0, -1, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 530 : i64, name = "v_in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 528 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 527 : i64, name = "__local_delta_476", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 533 : i64, name = "u_in", negateOffset = false, offset = [-1, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 534 : i64, name = "u_in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 532 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 531 : i64, name = "__local_delta_479", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.binary_operator {
                  iir.var_access_expr {
                  } attributes {id = 538 : i64, isExternal = false, name = "__local_delta_476"}
                } {
                  iir.var_access_expr {
                  } attributes {id = 539 : i64, isExternal = false, name = "frac_1_dy"}
                } attributes {id = 537 : i64, op = 2 : i64}
              } {
                iir.binary_operator {
                  iir.var_access_expr {
                  } attributes {id = 541 : i64, isExternal = false, name = "__local_delta_479"}
                } {
                  iir.var_access_expr {
                  } attributes {id = 542 : i64, isExternal = false, name = "frac_1_dx"}
                } attributes {id = 540 : i64, op = 2 : i64}
              } attributes {id = 536 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 535 : i64, name = "T_s", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 547 : i64, name = "T_sqr_s", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.var_access_expr {
                  } attributes {id = 545 : i64, isExternal = false, name = "T_s"}
                } {
                  iir.var_access_expr {
                  } attributes {id = 546 : i64, isExternal = false, name = "T_s"}
                } attributes {id = 544 : i64, op = 2 : i64}
              } attributes {id = 548 : i64, op = 0 : i64}
            } attributes {id = 543 : i64}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 551 : i64, name = "u_in", negateOffset = false, offset = [0, 1, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 552 : i64, name = "u_in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 550 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 549 : i64, name = "__local_delta_482", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 555 : i64, name = "v_in", negateOffset = false, offset = [1, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 556 : i64, name = "v_in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 554 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 553 : i64, name = "__local_delta_485", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.binary_operator {
                  iir.var_access_expr {
                  } attributes {id = 560 : i64, isExternal = false, name = "__local_delta_482"}
                } {
                  iir.var_access_expr {
                  } attributes {id = 561 : i64, isExternal = false, name = "frac_1_dy"}
                } attributes {id = 559 : i64, op = 2 : i64}
              } {
                iir.binary_operator {
                  iir.var_access_expr {
                  } attributes {id = 563 : i64, isExternal = false, name = "__local_delta_485"}
                } {
                  iir.var_access_expr {
                  } attributes {id = 564 : i64, isExternal = false, name = "frac_1_dx"}
                } attributes {id = 562 : i64, op = 2 : i64}
              } attributes {id = 558 : i64, op = 0 : i64}
            } attributes {dimension = 0 : i64, id = 557 : i64, name = "S_uv", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 569 : i64, name = "S_sqr_uv", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.var_access_expr {
                  } attributes {id = 567 : i64, isExternal = false, name = "S_uv"}
                } {
                  iir.var_access_expr {
                  } attributes {id = 568 : i64, isExternal = false, name = "S_uv"}
                } attributes {id = 566 : i64, op = 2 : i64}
              } attributes {id = 570 : i64, op = 0 : i64}
            } attributes {id = 565 : i64}
          } attributes {id = 518 : i64}
        } attributes {id = 15 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 471 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 574 : i64, name = "weight_smag", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 575 : i64, name = "hdmaskvel", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 573 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 572 : i64, name = "hdweight", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.literal_access_expr {builtinType = 4 : i64, id = 578 : i64, value = "0.5"}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 580 : i64, name = "T_sqr_s", negateOffset = false, offset = [1, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 581 : i64, name = "T_sqr_s", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 579 : i64, op = 0 : i64}
              } attributes {id = 577 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 576 : i64, name = "__local_avg_488", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.literal_access_expr {builtinType = 4 : i64, id = 584 : i64, value = "0.5"}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 586 : i64, name = "S_sqr_uv", negateOffset = false, offset = [0, -1, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 587 : i64, name = "S_sqr_uv", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 585 : i64, op = 0 : i64}
              } attributes {id = 583 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 582 : i64, name = "__local_avg_491", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 591 : i64, name = "tau_smag", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.function_call {
                    iir.binary_operator {
                      iir.var_access_expr {
                      } attributes {id = 594 : i64, isExternal = false, name = "__local_avg_488"}
                    } {
                      iir.var_access_expr {
                      } attributes {id = 595 : i64, isExternal = false, name = "__local_avg_491"}
                    } attributes {id = 593 : i64, op = 0 : i64}
                  } attributes {callee = "gridtools::clang::math::sqrt", id = 592 : i64}
                } attributes {id = 590 : i64, op = 2 : i64}
              } {
                iir.var_access_expr {
                } attributes {id = 596 : i64, isExternal = false, name = "hdweight"}
              } attributes {id = 589 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 588 : i64, name = "smag_u", op = 0 : i64, type = {builtinType = 4 : i64, is_const = false, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.var_access_expr {
                } attributes {id = 603 : i64, isExternal = false, name = "smag_u"}
              } {
                iir.function_call {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 599 : i64, value = "0.5"}
                  iir.function_call {
                    iir.literal_access_expr {builtinType = 4 : i64, id = 601 : i64, value = "0"}
                    iir.var_access_expr {
                    } attributes {id = 602 : i64, isExternal = false, name = "smag_u"}
                  } attributes {callee = "gridtools::clang::math::max", id = 600 : i64}
                } attributes {callee = "gridtools::clang::math::min", id = 598 : i64}
              } attributes {id = 604 : i64, op = 0 : i64}
            } attributes {id = 597 : i64}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.literal_access_expr {builtinType = 4 : i64, id = 607 : i64, value = "0.5"}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 609 : i64, name = "T_sqr_s", negateOffset = false, offset = [0, 1, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 610 : i64, name = "T_sqr_s", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 608 : i64, op = 0 : i64}
              } attributes {id = 606 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 605 : i64, name = "__local_avg_494", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.literal_access_expr {builtinType = 4 : i64, id = 613 : i64, value = "0.5"}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 615 : i64, name = "S_sqr_uv", negateOffset = false, offset = [-1, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 616 : i64, name = "S_sqr_uv", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 614 : i64, op = 0 : i64}
              } attributes {id = 612 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 611 : i64, name = "__local_avg_497", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 620 : i64, name = "tau_smag", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.function_call {
                    iir.binary_operator {
                      iir.var_access_expr {
                      } attributes {id = 623 : i64, isExternal = false, name = "__local_avg_494"}
                    } {
                      iir.var_access_expr {
                      } attributes {id = 624 : i64, isExternal = false, name = "__local_avg_497"}
                    } attributes {id = 622 : i64, op = 0 : i64}
                  } attributes {callee = "gridtools::clang::math::sqrt", id = 621 : i64}
                } attributes {id = 619 : i64, op = 2 : i64}
              } {
                iir.var_access_expr {
                } attributes {id = 625 : i64, isExternal = false, name = "hdweight"}
              } attributes {id = 618 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 617 : i64, name = "smag_v", op = 0 : i64, type = {builtinType = 4 : i64, is_const = false, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.var_access_expr {
                } attributes {id = 632 : i64, isExternal = false, name = "smag_v"}
              } {
                iir.function_call {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 628 : i64, value = "0.5"}
                  iir.function_call {
                    iir.literal_access_expr {builtinType = 4 : i64, id = 630 : i64, value = "0"}
                    iir.var_access_expr {
                    } attributes {id = 631 : i64, isExternal = false, name = "smag_v"}
                  } attributes {callee = "gridtools::clang::math::max", id = 629 : i64}
                } attributes {callee = "gridtools::clang::math::min", id = 627 : i64}
              } attributes {id = 633 : i64, op = 0 : i64}
            } attributes {id = 626 : i64}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 636 : i64, name = "u_in", negateOffset = false, offset = [0, 1, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 637 : i64, name = "u_in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 635 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 634 : i64, name = "__local_delta_503", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 640 : i64, name = "u_in", negateOffset = false, offset = [0, -1, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 641 : i64, name = "u_in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 639 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 638 : i64, name = "__local_delta_506", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 647 : i64, name = "u_in", negateOffset = false, offset = [1, 0, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 648 : i64, name = "u_in", negateOffset = false, offset = [-1, 0, 0]}
                    } attributes {id = 646 : i64, op = 0 : i64}
                  } {
                    iir.binary_operator {
                      iir.literal_access_expr {builtinType = 4 : i64, id = 650 : i64, value = "2"}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 651 : i64, name = "u_in", negateOffset = false, offset = [0, 0, 0]}
                    } attributes {id = 649 : i64, op = 2 : i64}
                  } attributes {id = 645 : i64, op = 1 : i64}
                } {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 653 : i64, name = "crlato", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 654 : i64, isExternal = false, name = "__local_delta_503"}
                  } attributes {id = 652 : i64, op = 2 : i64}
                } attributes {id = 644 : i64, op = 0 : i64}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 656 : i64, name = "crlatu", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.var_access_expr {
                  } attributes {id = 657 : i64, isExternal = false, name = "__local_delta_506"}
                } attributes {id = 655 : i64, op = 2 : i64}
              } attributes {id = 643 : i64, op = 0 : i64}
            } attributes {dimension = 0 : i64, id = 642 : i64, name = "__local_laplacian_500", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.var_access_expr {
              } attributes {id = 659 : i64, isExternal = false, name = "__local_laplacian_500"}
            } attributes {dimension = 0 : i64, id = 658 : i64, name = "lapu", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 662 : i64, name = "v_in", negateOffset = false, offset = [0, 1, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 663 : i64, name = "v_in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 661 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 660 : i64, name = "__local_delta_512", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 666 : i64, name = "v_in", negateOffset = false, offset = [0, -1, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 667 : i64, name = "v_in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 665 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 664 : i64, name = "__local_delta_515", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 673 : i64, name = "v_in", negateOffset = false, offset = [1, 0, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 674 : i64, name = "v_in", negateOffset = false, offset = [-1, 0, 0]}
                    } attributes {id = 672 : i64, op = 0 : i64}
                  } {
                    iir.binary_operator {
                      iir.literal_access_expr {builtinType = 4 : i64, id = 676 : i64, value = "2"}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 677 : i64, name = "v_in", negateOffset = false, offset = [0, 0, 0]}
                    } attributes {id = 675 : i64, op = 2 : i64}
                  } attributes {id = 671 : i64, op = 1 : i64}
                } {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 679 : i64, name = "crlavo", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 680 : i64, isExternal = false, name = "__local_delta_512"}
                  } attributes {id = 678 : i64, op = 2 : i64}
                } attributes {id = 670 : i64, op = 0 : i64}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 682 : i64, name = "crlavu", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.var_access_expr {
                  } attributes {id = 683 : i64, isExternal = false, name = "__local_delta_515"}
                } attributes {id = 681 : i64, op = 2 : i64}
              } attributes {id = 669 : i64, op = 0 : i64}
            } attributes {dimension = 0 : i64, id = 668 : i64, name = "__local_laplacian_509", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.var_access_expr {
              } attributes {id = 685 : i64, isExternal = false, name = "__local_laplacian_509"}
            } attributes {dimension = 0 : i64, id = 684 : i64, name = "lapv", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 692 : i64, name = "u_out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 688 : i64, name = "u_in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.binary_operator {
                    iir.var_access_expr {
                    } attributes {id = 690 : i64, isExternal = false, name = "smag_u"}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 691 : i64, isExternal = false, name = "lapu"}
                  } attributes {id = 689 : i64, op = 2 : i64}
                } attributes {id = 687 : i64, op = 0 : i64}
              } attributes {id = 693 : i64, op = 0 : i64}
            } attributes {id = 686 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 700 : i64, name = "v_out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 696 : i64, name = "v_in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.binary_operator {
                    iir.var_access_expr {
                    } attributes {id = 698 : i64, isExternal = false, name = "smag_v"}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 699 : i64, isExternal = false, name = "lapv"}
                  } attributes {id = 697 : i64, op = 2 : i64}
                } attributes {id = 695 : i64, op = 0 : i64}
              } attributes {id = 701 : i64, op = 0 : i64}
            } attributes {id = 694 : i64}
          } attributes {id = 571 : i64}
        } attributes {id = 16 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 473 : i64}
    } attributes {caches = {_201 = #iir.cache<accessID: 201, type: "IJ", policy: "Local">, _202 = #iir.cache<accessID: 202, type: "IJ", policy: "Local">}, id = 475 : i64, loopOrder = 0 : i64}
  } attributes {id = 203 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_203"}
    } attributes {id = 204 : i64}
  } attributes {id = 203 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_203"}
  } attributes {id = 204 : i64}
} attributes {accessIDToName = {_187 = "u_out", _188 = "v_out", _189 = "u_in", _190 = "v_in", _191 = "hdmaskvel", _192 = "crlavo", _193 = "crlavu", _194 = "crlato", _195 = "crlatu", _196 = "acrlat0", _197 = "eddlon", _198 = "eddlat", _199 = "tau_smag", _200 = "weight_smag", _201 = "T_sqr_s", _202 = "S_sqr_uv", _327 = "__local_frac_1_dx_327", _328 = "__local_frac_1_dy_328", _330 = "__local_T_s_330", _343 = "__local_S_uv_343", _356 = "__local_hdweight_356", _357 = "__local_smag_u_357", _378 = "__local_smag_v_378", _399 = "__local_lapu_399", _435 = "__local_lapv_435", _476 = "__local_delta_476", _479 = "__local_delta_479", _482 = "__local_delta_482", _485 = "__local_delta_485", _488 = "__local_avg_488", _491 = "__local_avg_491", _494 = "__local_avg_494", _497 = "__local_avg_497", _500 = "__local_laplacian_500", _503 = "__local_delta_503", _506 = "__local_delta_506", _509 = "__local_laplacian_509", _512 = "__local_delta_512", _515 = "__local_delta_515"}, accessIDType = {_187 = 6 : i64, _188 = 6 : i64, _189 = 6 : i64, _190 = 6 : i64, _191 = 6 : i64, _192 = 6 : i64, _193 = 6 : i64, _194 = 6 : i64, _195 = 6 : i64, _196 = 6 : i64, _197 = 6 : i64, _198 = 6 : i64, _199 = 6 : i64, _200 = 6 : i64, _201 = 3 : i64, _202 = 3 : i64}, allocatedFieldIDs = [], apiFieldIDs = [187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200], fieldAccessIDs = [187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202], fieldIDToLegalDimensions = {_187 = [1, 1, 1], _188 = [1, 1, 1], _189 = [1, 1, 1], _190 = [1, 1, 1], _191 = [1, 1, 1], _192 = [0, 1, 0], _193 = [0, 1, 0], _194 = [0, 1, 0], _195 = [0, 1, 0], _196 = [0, 1, 0], _197 = [1, 1, 1], _198 = [1, 1, 1], _199 = [1, 1, 1], _200 = [1, 1, 1], _201 = [1, 1, 1], _202 = [1, 1, 1]}, filename = "../../hd_smagorinsky.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__329 = "6371229", __366 = "0.5", __375 = "0.5", __376 = "0.5", __377 = "0", __387 = "0.5", __396 = "0.5", __397 = "0.5", __398 = "0", __422 = "2", __458 = "2"}, stencilName = "hd_smagorinsky_stencil", temporaryFieldIDs = [201, 202], variableVersions = {}}
