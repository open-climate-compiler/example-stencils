iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 185 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.binary_operator {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 180 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                      } {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 181 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                      } attributes {id = 179 : i64, op = 0 : i64}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 182 : i64, name = "in", negateOffset = false, offset = [0, 0, 2]}
                    } attributes {id = 178 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 183 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                  } attributes {id = 177 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 184 : i64, name = "in", negateOffset = false, offset = [0, 0, -2]}
                } attributes {id = 176 : i64, op = 0 : i64}
              } attributes {id = 186 : i64, op = 0 : i64}
            } attributes {id = 175 : i64}
          } attributes {id = 174 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 2, 0, 2>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 198 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.binary_operator {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 193 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                      } {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 194 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                      } attributes {id = 192 : i64, op = 0 : i64}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 195 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                    } attributes {id = 191 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 196 : i64, name = "in", negateOffset = false, offset = [0, 0, 2]}
                  } attributes {id = 190 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 197 : i64, name = "out", negateOffset = false, offset = [0, 0, -1]}
                } attributes {id = 189 : i64, op = 0 : i64}
              } attributes {id = 199 : i64, op = 0 : i64}
            } attributes {id = 188 : i64}
          } attributes {id = 187 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 3, 1048576, -2>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 209 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 205 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 206 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                    } attributes {id = 204 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 207 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                  } attributes {id = 203 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 208 : i64, name = "out", negateOffset = false, offset = [0, 0, -1]}
                } attributes {id = 202 : i64, op = 0 : i64}
              } attributes {id = 210 : i64, op = 0 : i64}
            } attributes {id = 201 : i64}
          } attributes {id = 200 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<1048576, -1, 1048576, -1>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 218 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 215 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 216 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                  } attributes {id = 214 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 217 : i64, name = "out", negateOffset = false, offset = [0, 0, -1]}
                } attributes {id = 213 : i64, op = 0 : i64}
              } attributes {id = 219 : i64, op = 0 : i64}
            } attributes {id = 212 : i64}
          } attributes {id = 211 : i64}
        } attributes {id = 3 : i64, interval = #iir.interval<1048576, 0, 1048576, 0>}
      } attributes {id = 109 : i64}
    } attributes {caches = {_53 = #iir.cache<accessID: 53, type: "K", policy: "Fill", interval: [0, 2, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>, _54 = #iir.cache<accessID: 54, type: "K", policy: "Flush", interval: [0, 2, 1048576, 0], enclosingAccessInterval: [0, 2, 1048576, 0], window: [0, 0]>}, id = 120 : i64, loopOrder = 1 : i64}
  } attributes {id = 55 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_55"}
    } attributes {id = 56 : i64}
  } attributes {id = 55 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_55"}
  } attributes {id = 56 : i64}
} attributes {accessIDToName = {_53 = "in", _54 = "out"}, accessIDType = {_53 = 6 : i64, _54 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [53, 54], fieldAccessIDs = [53, 54], fieldIDToLegalDimensions = {_53 = [1, 1, 1], _54 = [1, 1, 1]}, filename = "../../kcache_fill.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "kcache_fill", temporaryFieldIDs = [], variableVersions = {}}
