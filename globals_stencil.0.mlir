iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.if {
              iir.expr_statement {
                iir.var_access_expr {
                } attributes {id = 46 : i64, isExternal = true, name = "var_compiletime"}
              } attributes {id = 45 : i64}
            } {
              iir.block_statement {
                iir.expr_statement {
                  iir.assignment_expr {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 54 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.binary_operator {
                      iir.binary_operator {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 51 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                      } {
                        iir.var_access_expr {
                        } attributes {id = 52 : i64, isExternal = true, name = "var_runtime"}
                      } attributes {id = 50 : i64, op = 0 : i64}
                    } {
                      iir.var_access_expr {
                      } attributes {id = 53 : i64, isExternal = true, name = "var_default"}
                    } attributes {id = 49 : i64, op = 0 : i64}
                  } attributes {id = 55 : i64, op = 0 : i64}
                } attributes {id = 48 : i64}
              } attributes {id = 47 : i64}
            } {
            } attributes {id = 44 : i64}
          } attributes {id = 43 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 40 : i64}
    } attributes {caches = {}, id = 42 : i64, loopOrder = 0 : i64}
  } attributes {id = 22 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_22"}
    } attributes {id = 23 : i64}
  } attributes {id = 22 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_22"}
  } attributes {id = 23 : i64}
} attributes {accessIDToName = {_17 = "var_compiletime", _18 = "var_runtime", _19 = "var_default", _20 = "in", _21 = "out"}, accessIDType = {_17 = 0 : i64, _18 = 0 : i64, _19 = 0 : i64, _20 = 6 : i64, _21 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [20, 21], fieldAccessIDs = [20, 21], fieldIDToLegalDimensions = {_20 = [1, 1, 1], _21 = [1, 1, 1]}, filename = "../../globals_stencil.cpp", globalVariableIDs = [17, 18, 19], globalVariableToValue = {var_compiletime = {type = 0 : i64, valueIsSet = false}, var_default = {type = 2 : i64, value = 2.000000e+00 : f64, valueIsSet = true}, var_runtime = {type = 1 : i64, valueIsSet = false}}, literalIDToName = {}, stencilName = "globals_stencil", temporaryFieldIDs = [], variableVersions = {}}
