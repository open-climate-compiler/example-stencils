iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.if {
              iir.expr_statement {
                iir.binary_operator {
                  iir.var_access_expr {
                  } attributes {id = 120 : i64, isExternal = true, name = "var1"}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 121 : i64, value = "1"}
                } attributes {id = 119 : i64, op = 8 : i64}
              } attributes {id = 118 : i64}
            } {
              iir.block_statement {
                iir.expr_statement {
                  iir.assignment_expr {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 125 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 124 : i64, name = "in", negateOffset = false, offset = [1, 0, 0]}
                  } attributes {id = 126 : i64, op = 0 : i64}
                } attributes {id = 123 : i64}
              } attributes {id = 122 : i64}
            } {
              iir.block_statement {
                iir.expr_statement {
                  iir.assignment_expr {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 130 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 129 : i64, name = "in", negateOffset = false, offset = [-1, 0, 0]}
                  } attributes {id = 131 : i64, op = 0 : i64}
                } attributes {id = 128 : i64}
              } attributes {id = 127 : i64}
            } attributes {id = 117 : i64}
            iir.if {
              iir.expr_statement {
                iir.binary_operator {
                  iir.var_access_expr {
                  } attributes {id = 135 : i64, isExternal = true, name = "var1"}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 136 : i64, value = "1"}
                } attributes {id = 134 : i64, op = 8 : i64}
              } attributes {id = 133 : i64}
            } {
              iir.block_statement {
                iir.expr_statement {
                  iir.assignment_expr {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 140 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 139 : i64, name = "in", negateOffset = false, offset = [0, 1, 0]}
                  } attributes {id = 141 : i64, op = 0 : i64}
                } attributes {id = 138 : i64}
              } attributes {id = 137 : i64}
            } {
              iir.block_statement {
                iir.expr_statement {
                  iir.assignment_expr {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 145 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 144 : i64, name = "in", negateOffset = false, offset = [0, -1, 0]}
                  } attributes {id = 146 : i64, op = 0 : i64}
                } attributes {id = 143 : i64}
              } attributes {id = 142 : i64}
            } attributes {id = 132 : i64}
          } attributes {id = 116 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 77 : i64}
    } attributes {caches = {}, id = 115 : i64, loopOrder = 0 : i64}
  } attributes {id = 56 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_56"}
    } attributes {id = 57 : i64}
  } attributes {id = 56 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_56"}
  } attributes {id = 57 : i64}
} attributes {accessIDToName = {_52 = "var2", _53 = "var1", _54 = "in", _55 = "out"}, accessIDType = {_52 = 0 : i64, _53 = 0 : i64, _54 = 6 : i64, _55 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [54, 55], fieldAccessIDs = [54, 55], fieldIDToLegalDimensions = {_54 = [1, 1, 1], _55 = [1, 1, 1]}, filename = "../../conditional_stencil.cpp", globalVariableIDs = [52, 53], globalVariableToValue = {var1 = {type = 1 : i64, value = 1 : i64, valueIsSet = true}, var2 = {type = 0 : i64, valueIsSet = false}}, literalIDToName = {__114 = "1", __96 = "1"}, stencilName = "conditional_stencil", temporaryFieldIDs = [], variableVersions = {}}
