iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 92 : i64, name = "lap", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.binary_operator {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 85 : i64, name = "u", negateOffset = false, offset = [1, 0, 0]}
                      } {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 86 : i64, name = "u", negateOffset = false, offset = [-1, 0, 0]}
                      } attributes {id = 84 : i64, op = 0 : i64}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 87 : i64, name = "u", negateOffset = false, offset = [0, 1, 0]}
                    } attributes {id = 83 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 88 : i64, name = "u", negateOffset = false, offset = [0, -1, 0]}
                  } attributes {id = 82 : i64, op = 0 : i64}
                } {
                  iir.binary_operator {
                    iir.literal_access_expr {builtinType = 4 : i64, id = 90 : i64, value = "4"}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 91 : i64, name = "u", negateOffset = false, offset = [0, 0, 0]}
                  } attributes {id = 89 : i64, op = 2 : i64}
                } attributes {id = 81 : i64, op = 1 : i64}
              } attributes {id = 93 : i64, op = 0 : i64}
            } attributes {id = 80 : i64}
          } attributes {id = 79 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 74 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 107 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.binary_operator {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 100 : i64, name = "lap", negateOffset = false, offset = [1, 0, 0]}
                      } {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 101 : i64, name = "lap", negateOffset = false, offset = [-1, 0, 0]}
                      } attributes {id = 99 : i64, op = 0 : i64}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 102 : i64, name = "lap", negateOffset = false, offset = [0, 1, 0]}
                    } attributes {id = 98 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 103 : i64, name = "lap", negateOffset = false, offset = [0, -1, 0]}
                  } attributes {id = 97 : i64, op = 0 : i64}
                } {
                  iir.binary_operator {
                    iir.literal_access_expr {builtinType = 4 : i64, id = 105 : i64, value = "4"}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 106 : i64, name = "lap", negateOffset = false, offset = [0, 0, 0]}
                  } attributes {id = 104 : i64, op = 2 : i64}
                } attributes {id = 96 : i64, op = 1 : i64}
              } attributes {id = 108 : i64, op = 0 : i64}
            } attributes {id = 95 : i64}
          } attributes {id = 94 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 76 : i64}
    } attributes {caches = {_35 = #iir.cache<accessID: 35, type: "IJ", policy: "Local">}, id = 78 : i64, loopOrder = 0 : i64}
  } attributes {id = 36 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_36"}
    } attributes {id = 37 : i64}
  } attributes {id = 36 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_36"}
  } attributes {id = 37 : i64}
} attributes {accessIDToName = {_33 = "u", _34 = "out", _35 = "lap"}, accessIDType = {_33 = 6 : i64, _34 = 6 : i64, _35 = 3 : i64}, allocatedFieldIDs = [], apiFieldIDs = [33, 34], fieldAccessIDs = [33, 34, 35], fieldIDToLegalDimensions = {_33 = [1, 1, 1], _34 = [1, 1, 1], _35 = [1, 1, 1]}, filename = "../../hori_diff_stencil_01.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__72 = "4", __73 = "4"}, stencilName = "hori_diff_stencil", temporaryFieldIDs = [35], variableVersions = {}}
