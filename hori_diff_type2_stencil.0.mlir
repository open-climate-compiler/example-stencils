iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 431 : i64, name = "in", negateOffset = false, offset = [0, 1, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 432 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 430 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 429 : i64, name = "__local_delta_386", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 435 : i64, name = "in", negateOffset = false, offset = [0, -1, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 436 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 434 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 433 : i64, name = "__local_delta_389", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 442 : i64, name = "in", negateOffset = false, offset = [1, 0, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 443 : i64, name = "in", negateOffset = false, offset = [-1, 0, 0]}
                    } attributes {id = 441 : i64, op = 0 : i64}
                  } {
                    iir.binary_operator {
                      iir.literal_access_expr {builtinType = 4 : i64, id = 445 : i64, value = "2"}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 446 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                    } attributes {id = 444 : i64, op = 2 : i64}
                  } attributes {id = 440 : i64, op = 1 : i64}
                } {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 448 : i64, name = "crlato", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 449 : i64, isExternal = false, name = "__local_delta_386"}
                  } attributes {id = 447 : i64, op = 2 : i64}
                } attributes {id = 439 : i64, op = 0 : i64}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 451 : i64, name = "crlatu", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.var_access_expr {
                  } attributes {id = 452 : i64, isExternal = false, name = "__local_delta_389"}
                } attributes {id = 450 : i64, op = 2 : i64}
              } attributes {id = 438 : i64, op = 0 : i64}
            } attributes {dimension = 0 : i64, id = 437 : i64, name = "__local_laplacian_383", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 455 : i64, name = "lap", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.var_access_expr {
                } attributes {id = 454 : i64, isExternal = false, name = "__local_laplacian_383"}
              } attributes {id = 456 : i64, op = 0 : i64}
            } attributes {id = 453 : i64}
          } attributes {id = 428 : i64}
        } attributes {id = 16 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 378 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 460 : i64, name = "lap", negateOffset = false, offset = [1, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 461 : i64, name = "lap", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 459 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 458 : i64, name = "__local_delta_392", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.var_access_expr {
              } attributes {id = 463 : i64, isExternal = false, name = "__local_delta_392"}
            } attributes {dimension = 0 : i64, id = 462 : i64, name = "flx", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 466 : i64, name = "in", negateOffset = false, offset = [1, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 467 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 465 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 464 : i64, name = "__local_delta_398", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.ternary_operator {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.var_access_expr {
                    } attributes {id = 472 : i64, isExternal = false, name = "__local_flx_262"}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 473 : i64, isExternal = false, name = "__local_delta_398"}
                  } attributes {id = 471 : i64, op = 2 : i64}
                } {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 474 : i64, value = "0"}
                } attributes {id = 470 : i64, op = 5 : i64}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 475 : i64, value = "0"}
              } {
                iir.var_access_expr {
                } attributes {id = 476 : i64, isExternal = false, name = "__local_flx_262"}
              } attributes {id = 469 : i64}
            } attributes {dimension = 0 : i64, id = 468 : i64, name = "__local_diffusive_flux_x_395", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 479 : i64, name = "lap", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 480 : i64, name = "lap", negateOffset = false, offset = [-1, 0, 0]}
              } attributes {id = 478 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 477 : i64, name = "__local_delta_401", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.var_access_expr {
              } attributes {id = 482 : i64, isExternal = false, name = "__local_delta_401"}
            } attributes {dimension = 0 : i64, id = 481 : i64, name = "flx", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 485 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 486 : i64, name = "in", negateOffset = false, offset = [-1, 0, 0]}
              } attributes {id = 484 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 483 : i64, name = "__local_delta_407", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.ternary_operator {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.var_access_expr {
                    } attributes {id = 491 : i64, isExternal = false, name = "__local_flx_294"}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 492 : i64, isExternal = false, name = "__local_delta_407"}
                  } attributes {id = 490 : i64, op = 2 : i64}
                } {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 493 : i64, value = "0"}
                } attributes {id = 489 : i64, op = 5 : i64}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 494 : i64, value = "0"}
              } {
                iir.var_access_expr {
                } attributes {id = 495 : i64, isExternal = false, name = "__local_flx_294"}
              } attributes {id = 488 : i64}
            } attributes {dimension = 0 : i64, id = 487 : i64, name = "__local_diffusive_flux_x_404", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.var_access_expr {
                } attributes {id = 498 : i64, isExternal = false, name = "__local_diffusive_flux_x_395"}
              } {
                iir.var_access_expr {
                } attributes {id = 499 : i64, isExternal = false, name = "__local_diffusive_flux_x_404"}
              } attributes {id = 497 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 496 : i64, name = "delta_flux_x", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 502 : i64, name = "lap", negateOffset = false, offset = [0, 1, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 503 : i64, name = "lap", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 501 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 500 : i64, name = "__local_delta_410", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 506 : i64, name = "crlato", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.var_access_expr {
                } attributes {id = 507 : i64, isExternal = false, name = "__local_delta_410"}
              } attributes {id = 505 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 504 : i64, name = "fly", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 510 : i64, name = "in", negateOffset = false, offset = [0, 1, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 511 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 509 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 508 : i64, name = "__local_delta_416", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.ternary_operator {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.var_access_expr {
                    } attributes {id = 516 : i64, isExternal = false, name = "__local_fly_329"}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 517 : i64, isExternal = false, name = "__local_delta_416"}
                  } attributes {id = 515 : i64, op = 2 : i64}
                } {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 518 : i64, value = "0"}
                } attributes {id = 514 : i64, op = 5 : i64}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 519 : i64, value = "0"}
              } {
                iir.var_access_expr {
                } attributes {id = 520 : i64, isExternal = false, name = "__local_fly_329"}
              } attributes {id = 513 : i64}
            } attributes {dimension = 0 : i64, id = 512 : i64, name = "__local_diffusive_flux_y_413", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 523 : i64, name = "lap", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 524 : i64, name = "lap", negateOffset = false, offset = [0, -1, 0]}
              } attributes {id = 522 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 521 : i64, name = "__local_delta_419", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 527 : i64, name = "crlato", negateOffset = false, offset = [0, -1, 0]}
              } {
                iir.var_access_expr {
                } attributes {id = 528 : i64, isExternal = false, name = "__local_delta_419"}
              } attributes {id = 526 : i64, op = 2 : i64}
            } attributes {dimension = 0 : i64, id = 525 : i64, name = "fly", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 531 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 532 : i64, name = "in", negateOffset = false, offset = [0, -1, 0]}
              } attributes {id = 530 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 529 : i64, name = "__local_delta_425", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.ternary_operator {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.var_access_expr {
                    } attributes {id = 537 : i64, isExternal = false, name = "__local_fly_363"}
                  } {
                    iir.var_access_expr {
                    } attributes {id = 538 : i64, isExternal = false, name = "__local_delta_425"}
                  } attributes {id = 536 : i64, op = 2 : i64}
                } {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 539 : i64, value = "0"}
                } attributes {id = 535 : i64, op = 5 : i64}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 540 : i64, value = "0"}
              } {
                iir.var_access_expr {
                } attributes {id = 541 : i64, isExternal = false, name = "__local_fly_363"}
              } attributes {id = 534 : i64}
            } attributes {dimension = 0 : i64, id = 533 : i64, name = "__local_diffusive_flux_y_422", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.var_access_expr {
                } attributes {id = 544 : i64, isExternal = false, name = "__local_diffusive_flux_y_413"}
              } {
                iir.var_access_expr {
                } attributes {id = 545 : i64, isExternal = false, name = "__local_diffusive_flux_y_422"}
              } attributes {id = 543 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 542 : i64, name = "delta_flux_y", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 554 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 548 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 550 : i64, name = "hdmask", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.binary_operator {
                      iir.var_access_expr {
                      } attributes {id = 552 : i64, isExternal = false, name = "delta_flux_x"}
                    } {
                      iir.var_access_expr {
                      } attributes {id = 553 : i64, isExternal = false, name = "delta_flux_y"}
                    } attributes {id = 551 : i64, op = 0 : i64}
                  } attributes {id = 549 : i64, op = 2 : i64}
                } attributes {id = 547 : i64, op = 1 : i64}
              } attributes {id = 555 : i64, op = 0 : i64}
            } attributes {id = 546 : i64}
          } attributes {id = 457 : i64}
        } attributes {id = 17 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 380 : i64}
    } attributes {caches = {_165 = #iir.cache<accessID: 165, type: "IJ", policy: "Local">}, id = 382 : i64, loopOrder = 0 : i64}
  } attributes {id = 166 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_166"}
    } attributes {id = 167 : i64}
  } attributes {id = 166 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_166"}
  } attributes {id = 167 : i64}
} attributes {accessIDToName = {_160 = "out", _161 = "in", _162 = "crlato", _163 = "crlatu", _164 = "hdmask", _165 = "lap", _244 = "__local_delta_flux_x_244", _262 = "__local_flx_262", _294 = "__local_flx_294", _309 = "__local_delta_flux_y_309", _329 = "__local_fly_329", _363 = "__local_fly_363", _383 = "__local_laplacian_383", _386 = "__local_delta_386", _389 = "__local_delta_389", _392 = "__local_delta_392", _395 = "__local_diffusive_flux_x_395", _398 = "__local_delta_398", _401 = "__local_delta_401", _404 = "__local_diffusive_flux_x_404", _407 = "__local_delta_407", _410 = "__local_delta_410", _413 = "__local_diffusive_flux_y_413", _416 = "__local_delta_416", _419 = "__local_delta_419", _422 = "__local_diffusive_flux_y_422", _425 = "__local_delta_425"}, accessIDType = {_160 = 6 : i64, _161 = 6 : i64, _162 = 6 : i64, _163 = 6 : i64, _164 = 6 : i64, _165 = 3 : i64}, allocatedFieldIDs = [], apiFieldIDs = [160, 161, 162, 163, 164], fieldAccessIDs = [160, 161, 162, 163, 164, 165], fieldIDToLegalDimensions = {_160 = [1, 1, 1], _161 = [1, 1, 1], _162 = [0, 1, 0], _163 = [0, 1, 0], _164 = [1, 1, 1], _165 = [1, 1, 1]}, filename = "../../hori_diff_type2_stencil.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__231 = "2", __275 = "0", __276 = "0", __307 = "0", __308 = "0", __342 = "0", __343 = "0", __376 = "0", __377 = "0"}, stencilName = "hori_diff_type2_stencil", temporaryFieldIDs = [165], variableVersions = {}}
