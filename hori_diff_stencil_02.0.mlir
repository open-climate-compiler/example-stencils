iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.var_decl_statement {
              iir.binary_operator {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 109 : i64, name = "u", negateOffset = false, offset = [1, 0, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 110 : i64, name = "u", negateOffset = false, offset = [-1, 0, 0]}
                    } attributes {id = 108 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 111 : i64, name = "u", negateOffset = false, offset = [0, 1, 0]}
                  } attributes {id = 107 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 112 : i64, name = "u", negateOffset = false, offset = [0, -1, 0]}
                } attributes {id = 106 : i64, op = 0 : i64}
              } {
                iir.binary_operator {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 114 : i64, value = "4"}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 115 : i64, name = "u", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 113 : i64, op = 2 : i64}
              } attributes {id = 105 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 104 : i64, name = "__local_laplacian_97", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 118 : i64, name = "lap", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.var_access_expr {
                } attributes {id = 117 : i64, isExternal = false, name = "__local_laplacian_97"}
              } attributes {id = 119 : i64, op = 0 : i64}
            } attributes {id = 116 : i64}
          } attributes {id = 103 : i64}
        } attributes {id = 3 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 92 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.var_decl_statement {
              iir.binary_operator {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 126 : i64, name = "lap", negateOffset = false, offset = [1, 0, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 127 : i64, name = "lap", negateOffset = false, offset = [-1, 0, 0]}
                    } attributes {id = 125 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 128 : i64, name = "lap", negateOffset = false, offset = [0, 1, 0]}
                  } attributes {id = 124 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 129 : i64, name = "lap", negateOffset = false, offset = [0, -1, 0]}
                } attributes {id = 123 : i64, op = 0 : i64}
              } {
                iir.binary_operator {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 131 : i64, value = "4"}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 132 : i64, name = "lap", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 130 : i64, op = 2 : i64}
              } attributes {id = 122 : i64, op = 1 : i64}
            } attributes {dimension = 0 : i64, id = 121 : i64, name = "__local_laplacian_100", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 135 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.var_access_expr {
                } attributes {id = 134 : i64, isExternal = false, name = "__local_laplacian_100"}
              } attributes {id = 136 : i64, op = 0 : i64}
            } attributes {id = 133 : i64}
          } attributes {id = 120 : i64}
        } attributes {id = 4 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 94 : i64}
    } attributes {caches = {_43 = #iir.cache<accessID: 43, type: "IJ", policy: "Local">}, id = 96 : i64, loopOrder = 0 : i64}
  } attributes {id = 44 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_44"}
    } attributes {id = 45 : i64}
  } attributes {id = 44 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_44"}
  } attributes {id = 45 : i64}
} attributes {accessIDToName = {_100 = "__local_laplacian_100", _41 = "u", _42 = "out", _43 = "lap", _97 = "__local_laplacian_97"}, accessIDType = {_41 = 6 : i64, _42 = 6 : i64, _43 = 3 : i64}, allocatedFieldIDs = [], apiFieldIDs = [41, 42], fieldAccessIDs = [41, 42, 43], fieldIDToLegalDimensions = {_41 = [1, 1, 1], _42 = [1, 1, 1], _43 = [1, 1, 1]}, filename = "../../hori_diff_stencil_02.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__76 = "4", __91 = "4"}, stencilName = "hori_diff_stencil", temporaryFieldIDs = [43], variableVersions = {}}
