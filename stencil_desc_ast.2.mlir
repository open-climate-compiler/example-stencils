"iir.iir"() ( {
  "iir.stencil"() ( {
    "iir.multi_stage"() ( {
      "iir.stage"() ( {
        "iir.do_method"() ( {
          "iir.block_statement"() ( {
            "iir.expr_statement"() ( {
              "iir.assignment_expr"() ( {
                "iir.field_access_expr"() {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 695 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]} : () -> ()
                "iir._assignment_expr_end"() : () -> ()
              },  {
                "iir.binary_operator"() ( {
                  "iir.binary_operator"() ( {
                    "iir.field_access_expr"() {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 692 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]} : () -> ()
                    "iir._binary_operator_end"() : () -> ()
                  },  {
                    "iir.var_access_expr"() ( {
                      "iir._var_access_expr_end"() : () -> ()
                    }) {id = 693 : i64, isExternal = true, name = "var_runtime"} : () -> ()
                    "iir._binary_operator_end"() : () -> ()
                  }) {id = 691 : i64, op = 0 : i64} : () -> ()
                  "iir._binary_operator_end"() : () -> ()
                },  {
                  "iir.var_access_expr"() ( {
                    "iir._var_access_expr_end"() : () -> ()
                  }) {id = 694 : i64, isExternal = true, name = "var_compiletime"} : () -> ()
                  "iir._binary_operator_end"() : () -> ()
                }) {id = 690 : i64, op = 0 : i64} : () -> ()
                "iir._assignment_expr_end"() : () -> ()
              }) {id = 696 : i64, op = 0 : i64} : () -> ()
              "iir._expr_statement_end"() : () -> ()
            }) {id = 689 : i64} : () -> ()
            "iir._block_statement_end"() : () -> ()
          }) {id = 688 : i64} : () -> ()
          "iir._do_method_end"() : () -> ()
        }) {id = 2 : i64, interval = {lowerLevel = 0 : i64, lowerOffset = 0 : i64, upperLevel = 1048576 : i64, upperOffset = 0 : i64}} : () -> ()
        "iir._stage_end"() : () -> ()
      }) {id = 322 : i64} : () -> ()
      "iir._multi_stage_end"() : () -> ()
    }) {caches = {}, id = 687 : i64, loopOrder = 0 : i64} : () -> ()
    "iir._stencil_end"() : () -> ()
  }) {id = 319 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
},  {
  "iir.id_to_stencil_call_entry"() ( {
    "iir.stencil_call_decl_statement"() ( {
      "iir.stencil_call"() {arguments = [], callee = "__code_gen_319"} : () -> ()
      "iir._stencil_call_decl_statement_end"() : () -> ()
    }) {id = 320 : i64} : () -> ()
    "iir._id_to_stencil_call_entry_end"() : () -> ()
  }) {id = 319 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
},  {
  "iir.if"() ( {
    "iir.expr_statement"() ( {
      "iir.binary_operator"() ( {
        "iir.var_access_expr"() ( {
          "iir._var_access_expr_end"() : () -> ()
        }) {id = 293 : i64, isExternal = true, name = "var_runtime"} : () -> ()
        "iir._binary_operator_end"() : () -> ()
      },  {
        "iir.literal_access_expr"() {builtinType = 3 : i64, id = 294 : i64, value = "1"} : () -> ()
        "iir._binary_operator_end"() : () -> ()
      }) {id = 292 : i64, op = 8 : i64} : () -> ()
      "iir._expr_statement_end"() : () -> ()
    }) {id = 295 : i64} : () -> ()
    "iir._if_end"() : () -> ()
  },  {
    "iir.block_statement"() ( {
      "iir.if"() ( {
        "iir.expr_statement"() ( {
          "iir.binary_operator"() ( {
            "iir.var_access_expr"() ( {
              "iir._var_access_expr_end"() : () -> ()
            }) {id = 297 : i64, isExternal = true, name = "var_compiletime"} : () -> ()
            "iir._binary_operator_end"() : () -> ()
          },  {
            "iir.literal_access_expr"() {builtinType = 3 : i64, id = 298 : i64, value = "2"} : () -> ()
            "iir._binary_operator_end"() : () -> ()
          }) {id = 296 : i64, op = 8 : i64} : () -> ()
          "iir._expr_statement_end"() : () -> ()
        }) {id = 299 : i64} : () -> ()
        "iir._if_end"() : () -> ()
      },  {
        "iir.block_statement"() ( {
          "iir.stencil_call_decl_statement"() ( {
            "iir.stencil_call"() {arguments = [], callee = "__code_gen_319"} : () -> ()
            "iir._stencil_call_decl_statement_end"() : () -> ()
          }) {id = 320 : i64} : () -> ()
          "iir._block_statement_end"() : () -> ()
        }) {id = 310 : i64} : () -> ()
        "iir._if_end"() : () -> ()
      },  {
        "iir._if_end"() : () -> ()
      }) {id = 311 : i64} : () -> ()
      "iir._block_statement_end"() : () -> ()
    }) {id = 312 : i64} : () -> ()
    "iir._if_end"() : () -> ()
  },  {
    "iir._if_end"() : () -> ()
  }) {id = 313 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
}) {accessIDToName = {_286 = "var_compiletime", _287 = "var_runtime", _288 = "in", _289 = "out"}, accessIDType = {_286 = 0 : i64, _287 = 0 : i64, _288 = 6 : i64, _289 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [288, 289], fieldAccessIDs = [288, 289], fieldIDtoLegalDimensions = {_288 = [1, 1, 1], _289 = [1, 1, 1]}, filename = "stencil_desc_ast.cpp", globalVariableIDs = [286, 287], globalVariableToValue = {var_compiletime = {type = 1 : i64, valueIsSet = false}, var_runtime = {type = 1 : i64, value = 1 : i64, valueIsSet = true}}, literalIDToName = {__315 = "1", __318 = "2"}, stencilName = "test_03_stencil", temporaryFieldIDs = [], variableVersions = {}} : () -> ()
