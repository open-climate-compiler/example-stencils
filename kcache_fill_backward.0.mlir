iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 237 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.binary_operator {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 232 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                      } {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 233 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                      } attributes {id = 231 : i64, op = 0 : i64}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 234 : i64, name = "in", negateOffset = false, offset = [0, 0, -2]}
                    } attributes {id = 230 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 235 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                  } attributes {id = 229 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 236 : i64, name = "in", negateOffset = false, offset = [0, 0, 2]}
                } attributes {id = 228 : i64, op = 0 : i64}
              } attributes {id = 238 : i64, op = 0 : i64}
            } attributes {id = 227 : i64}
          } attributes {id = 226 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<1048576, -2, 1048576, -2>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 250 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.binary_operator {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 245 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                      } {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 246 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                      } attributes {id = 244 : i64, op = 0 : i64}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 247 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                    } attributes {id = 243 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 248 : i64, name = "in", negateOffset = false, offset = [0, 0, -2]}
                  } attributes {id = 242 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 249 : i64, name = "out", negateOffset = false, offset = [0, 0, 1]}
                } attributes {id = 241 : i64, op = 0 : i64}
              } attributes {id = 251 : i64, op = 0 : i64}
            } attributes {id = 240 : i64}
          } attributes {id = 239 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 2, 1048576, -3>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 261 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 257 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 258 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                    } attributes {id = 256 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 259 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                  } attributes {id = 255 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 260 : i64, name = "out", negateOffset = false, offset = [0, 0, 1]}
                } attributes {id = 254 : i64, op = 0 : i64}
              } attributes {id = 262 : i64, op = 0 : i64}
            } attributes {id = 253 : i64}
          } attributes {id = 252 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<0, 1, 0, 1>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 270 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 267 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 268 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                  } attributes {id = 266 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 269 : i64, name = "out", negateOffset = false, offset = [0, 0, 1]}
                } attributes {id = 265 : i64, op = 0 : i64}
              } attributes {id = 271 : i64, op = 0 : i64}
            } attributes {id = 264 : i64}
          } attributes {id = 263 : i64}
        } attributes {id = 3 : i64, interval = #iir.interval<0, 0, 0, 0>}
      } attributes {id = 109 : i64}
    } attributes {caches = {_120 = #iir.cache<accessID: 120, type: "K", policy: "Local", interval: [0, 0, 0, 1], enclosingAccessInterval: [0, 0, 0, 1]>, _121 = #iir.cache<accessID: 121, type: "K", policy: "Flush", interval: [0, 1, 1048576, -2], enclosingAccessInterval: [0, 1, 1048576, -2], window: [0, 0]>, _53 = #iir.cache<accessID: 53, type: "K", policy: "Fill", interval: [0, 0, 1048576, -2], enclosingAccessInterval: [0, 0, 1048576, 0]>}, id = 122 : i64, loopOrder = 2 : i64}
  } attributes {id = 55 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_55"}
    } attributes {id = 56 : i64}
  } attributes {id = 55 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_55"}
  } attributes {id = 56 : i64}
} attributes {accessIDToName = {_120 = "out_0", _121 = "out_1", _53 = "in", _54 = "out"}, accessIDType = {_120 = 3 : i64, _121 = 4 : i64, _53 = 6 : i64, _54 = 6 : i64}, allocatedFieldIDs = [121], apiFieldIDs = [53, 54], fieldAccessIDs = [53, 54, 120, 121], fieldIDToLegalDimensions = {_53 = [1, 1, 1], _54 = [1, 1, 1]}, filename = "../../kcache_fill_backward.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "kcache_fill_backward", temporaryFieldIDs = [120], variableVersions = {_54 = [120, 121]}}
