iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 169 : i64, name = "__tmp_delta_109", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 167 : i64, name = "in", negateOffset = false, offset = [1, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 168 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 166 : i64, op = 1 : i64}
              } attributes {id = 170 : i64, op = 0 : i64}
            } attributes {id = 165 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 175 : i64, name = "__tmp_delta_110", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 173 : i64, name = "in", negateOffset = false, offset = [0, 1, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 174 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 172 : i64, op = 1 : i64}
              } attributes {id = 176 : i64, op = 0 : i64}
            } attributes {id = 171 : i64}
            iir.var_decl_statement {
              iir.binary_operator {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 179 : i64, name = "__tmp_delta_109", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 180 : i64, name = "__tmp_delta_110", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 178 : i64, op = 0 : i64}
            } attributes {dimension = 0 : i64, id = 177 : i64, name = "__local_sum_161", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.var_decl_statement {
              iir.var_access_expr {
              } attributes {id = 182 : i64, isExternal = false, name = "__local_sum_161"}
            } attributes {dimension = 0 : i64, id = 181 : i64, name = "__local_delta_sum_152", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 185 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.var_access_expr {
                } attributes {id = 184 : i64, isExternal = false, name = "__local_delta_sum_152"}
              } attributes {id = 186 : i64, op = 0 : i64}
            } attributes {id = 183 : i64}
          } attributes {id = 164 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 79 : i64}
    } attributes {caches = {}, id = 151 : i64, loopOrder = 0 : i64}
  } attributes {id = 66 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_66"}
    } attributes {id = 67 : i64}
  } attributes {id = 66 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_66"}
  } attributes {id = 67 : i64}
} attributes {accessIDToName = {_109 = "__tmp_delta_109", _110 = "__tmp_delta_110", _152 = "__local_delta_sum_152", _161 = "__local_sum_161", _64 = "in", _65 = "out"}, accessIDType = {_109 = 3 : i64, _110 = 3 : i64, _64 = 6 : i64, _65 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [64, 65], fieldAccessIDs = [64, 65, 109, 110], fieldIDToLegalDimensions = {_64 = [1, 1, 1], _65 = [1, 1, 1]}, filename = "../../nested_stencil_functions.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "test_04_stencil", temporaryFieldIDs = [109, 110], variableVersions = {}}
