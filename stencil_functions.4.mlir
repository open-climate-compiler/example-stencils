"iir.iir"() ( {
  "iir.stencil"() ( {
    "iir.multi_stage"() ( {
      "iir.stage"() ( {
        "iir.do_method"() ( {
          "iir.block_statement"() ( {
            "iir.expr_statement"() ( {
              "iir.assignment_expr"() ( {
                "iir.field_access_expr"() {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 345 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]} : () -> ()
                "iir._assignment_expr_end"() : () -> ()
              },  {
                "iir.field_access_expr"() {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 344 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]} : () -> ()
                "iir._assignment_expr_end"() : () -> ()
              }) {id = 346 : i64, op = 0 : i64} : () -> ()
              "iir._expr_statement_end"() : () -> ()
            }) {id = 343 : i64} : () -> ()
            "iir._block_statement_end"() : () -> ()
          }) {id = 342 : i64} : () -> ()
          "iir._do_method_end"() : () -> ()
        }) {id = 12 : i64, interval = {lowerLevel = 0 : i64, lowerOffset = 0 : i64, upperLevel = 1048576 : i64, upperOffset = 0 : i64}} : () -> ()
        "iir._stage_end"() : () -> ()
      }) {id = 247 : i64} : () -> ()
      "iir._multi_stage_end"() : () -> ()
    }) {caches = {}, id = 341 : i64, loopOrder = 0 : i64} : () -> ()
    "iir._stencil_end"() : () -> ()
  }) {id = 237 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
},  {
  "iir.id_to_stencil_call_entry"() ( {
    "iir.stencil_call_decl_statement"() ( {
      "iir.stencil_call"() {arguments = [], callee = "__code_gen_237"} : () -> ()
      "iir._stencil_call_decl_statement_end"() : () -> ()
    }) {id = 238 : i64} : () -> ()
    "iir._id_to_stencil_call_entry_end"() : () -> ()
  }) {id = 237 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
},  {
  "iir.stencil_call_decl_statement"() ( {
    "iir.stencil_call"() {arguments = [], callee = "__code_gen_237"} : () -> ()
    "iir._stencil_call_decl_statement_end"() : () -> ()
  }) {id = 238 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
}) {accessIDToName = {_235 = "in", _236 = "out"}, accessIDType = {_235 = 6 : i64, _236 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [235, 236], fieldAccessIDs = [235, 236], fieldIDtoLegalDimensions = {_235 = [1, 1, 1], _236 = [1, 1, 1]}, filename = "stencil_functions.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "test_07_stencil", temporaryFieldIDs = [], variableVersions = {}} : () -> ()
