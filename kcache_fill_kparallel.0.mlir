iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 77 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 75 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 76 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                } attributes {id = 74 : i64, op = 0 : i64}
              } attributes {id = 78 : i64, op = 0 : i64}
            } attributes {id = 73 : i64}
          } attributes {id = 72 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 0, 1>}
      } attributes {id = 61 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 86 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 83 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 84 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                  } attributes {id = 82 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 85 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                } attributes {id = 81 : i64, op = 0 : i64}
              } attributes {id = 87 : i64, op = 0 : i64}
            } attributes {id = 80 : i64}
          } attributes {id = 79 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 1, 1048576, -1>}
      } attributes {id = 64 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 93 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 91 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 92 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                } attributes {id = 90 : i64, op = 0 : i64}
              } attributes {id = 94 : i64, op = 0 : i64}
            } attributes {id = 89 : i64}
          } attributes {id = 88 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<1048576, -1, 1048576, 0>}
      } attributes {id = 67 : i64}
    } attributes {caches = {_29 = #iir.cache<accessID: 29, type: "K", policy: "Fill", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>}, id = 69 : i64, loopOrder = 0 : i64}
  } attributes {id = 31 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_31"}
    } attributes {id = 32 : i64}
  } attributes {id = 31 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_31"}
  } attributes {id = 32 : i64}
} attributes {accessIDToName = {_29 = "in", _30 = "out"}, accessIDType = {_29 = 6 : i64, _30 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [29, 30], fieldAccessIDs = [29, 30], fieldIDToLegalDimensions = {_29 = [1, 1, 1], _30 = [1, 1, 1]}, filename = "../../kcache_fill_kparallel.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "kcache_fill_kparallel", temporaryFieldIDs = [], variableVersions = {}}
