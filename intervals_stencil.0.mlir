iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 76 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 74 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 75 : i64, value = "3"}
                } attributes {id = 73 : i64, op = 0 : i64}
              } attributes {id = 77 : i64, op = 0 : i64}
            } attributes {id = 72 : i64}
          } attributes {id = 71 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<4, 2, 1048576, 0>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 83 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 81 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 82 : i64, value = "2"}
                } attributes {id = 80 : i64, op = 0 : i64}
              } attributes {id = 84 : i64, op = 0 : i64}
            } attributes {id = 79 : i64}
          } attributes {id = 78 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<4, 1, 4, 1>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 90 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 88 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 89 : i64, value = "1"}
                } attributes {id = 87 : i64, op = 0 : i64}
              } attributes {id = 91 : i64, op = 0 : i64}
            } attributes {id = 86 : i64}
          } attributes {id = 85 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 4, 0>}
      } attributes {id = 65 : i64}
    } attributes {caches = {}, id = 68 : i64, loopOrder = 0 : i64}
  } attributes {id = 29 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_29"}
    } attributes {id = 30 : i64}
  } attributes {id = 29 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_29"}
  } attributes {id = 30 : i64}
} attributes {accessIDToName = {_27 = "in", _28 = "out"}, accessIDType = {_27 = 6 : i64, _28 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [27, 28], fieldAccessIDs = [27, 28], fieldIDToLegalDimensions = {_27 = [1, 1, 1], _28 = [1, 1, 1]}, filename = "../../intervals_stencil.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__59 = "1", __63 = "2", __67 = "3"}, stencilName = "intervals_stencil", temporaryFieldIDs = [], variableVersions = {}}
