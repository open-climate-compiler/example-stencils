All the files in this repository are translations of the code generation
examples that can be found [here](https://github.com/MeteoSwiss-APN/dawn/tree/master/gtclang/test/integration-test/CodeGen).