iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 95 : i64, name = "tmp", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 93 : i64, name = "in", negateOffset = false, offset = [1, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 94 : i64, name = "in", negateOffset = false, offset = [0, -2, 0]}
                } attributes {id = 92 : i64, op = 0 : i64}
              } attributes {id = 96 : i64, op = 0 : i64}
            } attributes {id = 91 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 99 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 3 : i64, id = 98 : i64, value = "1"}
              } attributes {id = 100 : i64, op = 0 : i64}
            } attributes {id = 97 : i64}
          } attributes {id = 90 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 0, 0>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 106 : i64, name = "tmp", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 104 : i64, name = "in", negateOffset = false, offset = [-3, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 105 : i64, name = "in", negateOffset = false, offset = [0, 1, 0]}
                } attributes {id = 103 : i64, op = 0 : i64}
              } attributes {id = 107 : i64, op = 0 : i64}
            } attributes {id = 102 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 110 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 109 : i64, name = "tmp", negateOffset = false, offset = [0, 0, -1]}
              } attributes {id = 111 : i64, op = 0 : i64}
            } attributes {id = 108 : i64}
          } attributes {id = 101 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 1, 1048576, 0>}
      } attributes {id = 58 : i64}
    } attributes {caches = {_29 = #iir.cache<accessID: 29, type: "K", policy: "Local", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>}, id = 64 : i64, loopOrder = 1 : i64}
  } attributes {id = 30 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_30"}
    } attributes {id = 31 : i64}
  } attributes {id = 30 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_30"}
  } attributes {id = 31 : i64}
} attributes {accessIDToName = {_27 = "in", _28 = "out", _29 = "tmp"}, accessIDType = {_27 = 6 : i64, _28 = 6 : i64, _29 = 3 : i64}, allocatedFieldIDs = [], apiFieldIDs = [27, 28], fieldAccessIDs = [27, 28, 29], fieldIDToLegalDimensions = {_27 = [1, 1, 1], _28 = [1, 1, 1], _29 = [1, 1, 1]}, filename = "../../asymmetric.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__60 = "1"}, stencilName = "asymmetric_stencil", temporaryFieldIDs = [29], variableVersions = {}}
