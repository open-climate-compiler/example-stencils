"iir.iir"() ( {
  "iir.stencil"() ( {
    "iir.multi_stage"() ( {
      "iir.stage"() ( {
        "iir.do_method"() ( {
          "iir.block_statement"() ( {
            "iir.var_decl_statement"() ( {
              "iir.binary_operator"() ( {
                "iir.field_access_expr"() {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 312 : i64, name = "in", negateOffset = false, offset = [1, 0, 0]} : () -> ()
                "iir._binary_operator_end"() : () -> ()
              },  {
                "iir.field_access_expr"() {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 313 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]} : () -> ()
                "iir._binary_operator_end"() : () -> ()
              }) {id = 311 : i64, op = 1 : i64} : () -> ()
              "iir._var_decl_statement_end"() : () -> ()
            }) {dimension = 0 : i64, id = 310 : i64, name = "__local_delta_306", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}} : () -> ()
            "iir.var_decl_statement"() ( {
              "iir.var_access_expr"() ( {
                "iir._var_access_expr_end"() : () -> ()
              }) {id = 315 : i64, isExternal = false, name = "__local_delta_306"} : () -> ()
              "iir._var_decl_statement_end"() : () -> ()
            }) {dimension = 0 : i64, id = 314 : i64, name = "__local_delta_nested_303", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}} : () -> ()
            "iir.expr_statement"() ( {
              "iir.assignment_expr"() ( {
                "iir.field_access_expr"() {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 318 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]} : () -> ()
                "iir._assignment_expr_end"() : () -> ()
              },  {
                "iir.var_access_expr"() ( {
                  "iir._var_access_expr_end"() : () -> ()
                }) {id = 317 : i64, isExternal = false, name = "__local_delta_nested_303"} : () -> ()
                "iir._assignment_expr_end"() : () -> ()
              }) {id = 319 : i64, op = 0 : i64} : () -> ()
              "iir._expr_statement_end"() : () -> ()
            }) {id = 316 : i64} : () -> ()
            "iir._block_statement_end"() : () -> ()
          }) {id = 309 : i64} : () -> ()
          "iir._do_method_end"() : () -> ()
        }) {id = 5 : i64, interval = {lowerLevel = 0 : i64, lowerOffset = 0 : i64, upperLevel = 1048576 : i64, upperOffset = 0 : i64}} : () -> ()
        "iir._stage_end"() : () -> ()
      }) {id = 192 : i64} : () -> ()
      "iir._multi_stage_end"() : () -> ()
    }) {caches = {}, id = 302 : i64, loopOrder = 0 : i64} : () -> ()
    "iir._stencil_end"() : () -> ()
  }) {id = 180 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
},  {
  "iir.id_to_stencil_call_entry"() ( {
    "iir.stencil_call_decl_statement"() ( {
      "iir.stencil_call"() {arguments = [], callee = "__code_gen_180"} : () -> ()
      "iir._stencil_call_decl_statement_end"() : () -> ()
    }) {id = 181 : i64} : () -> ()
    "iir._id_to_stencil_call_entry_end"() : () -> ()
  }) {id = 180 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
},  {
  "iir.stencil_call_decl_statement"() ( {
    "iir.stencil_call"() {arguments = [], callee = "__code_gen_180"} : () -> ()
    "iir._stencil_call_decl_statement_end"() : () -> ()
  }) {id = 181 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
}) {accessIDToName = {_178 = "in", _179 = "out", _303 = "__local_delta_nested_303", _306 = "__local_delta_306"}, accessIDType = {_178 = 6 : i64, _179 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [178, 179], fieldAccessIDs = [178, 179], fieldIDtoLegalDimensions = {_178 = [1, 1, 1], _179 = [1, 1, 1]}, filename = "stencil_functions.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "test_03_stencil", temporaryFieldIDs = [], variableVersions = {}} : () -> ()
