iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 55 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 53 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 54 : i64, value = "3"}
                } attributes {id = 52 : i64, op = 0 : i64}
              } attributes {id = 56 : i64, op = 0 : i64}
            } attributes {id = 51 : i64}
          } attributes {id = 50 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<4, 2, 1048576, -1>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 62 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 60 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 61 : i64, value = "1"}
                } attributes {id = 59 : i64, op = 0 : i64}
              } attributes {id = 63 : i64, op = 0 : i64}
            } attributes {id = 58 : i64}
          } attributes {id = 57 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 1, 4, 0>}
      } attributes {id = 45 : i64}
    } attributes {caches = {}, id = 48 : i64, loopOrder = 0 : i64}
  } attributes {id = 21 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_21"}
    } attributes {id = 22 : i64}
  } attributes {id = 21 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_21"}
  } attributes {id = 22 : i64}
} attributes {accessIDToName = {_19 = "in", _20 = "out"}, accessIDType = {_19 = 6 : i64, _20 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [19, 20], fieldAccessIDs = [19, 20], fieldIDToLegalDimensions = {_19 = [1, 1, 1], _20 = [1, 1, 1]}, filename = "../../intervals01.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__43 = "1", __47 = "3"}, stencilName = "intervals01", temporaryFieldIDs = [], variableVersions = {}}
