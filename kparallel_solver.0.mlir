iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 152 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 151 : i64, name = "a", negateOffset = false, offset = [0, 0, 1]}
              } attributes {id = 153 : i64, op = 0 : i64}
            } attributes {id = 150 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 158 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 156 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 157 : i64, value = "2.1000000000000001"}
                } attributes {id = 155 : i64, op = 2 : i64}
              } attributes {id = 159 : i64, op = 0 : i64}
            } attributes {id = 154 : i64}
          } attributes {id = 149 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 0, 0>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 163 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 162 : i64, name = "a", negateOffset = false, offset = [0, 0, -1]}
              } attributes {id = 164 : i64, op = 0 : i64}
            } attributes {id = 161 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 171 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 168 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.literal_access_expr {builtinType = 4 : i64, id = 169 : i64, value = "2"}
                  } attributes {id = 167 : i64, op = 2 : i64}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 170 : i64, value = "1"}
                } attributes {id = 166 : i64, op = 1 : i64}
              } attributes {id = 172 : i64, op = 0 : i64}
            } attributes {id = 165 : i64}
          } attributes {id = 160 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 1, 1048576, 0>}
      } attributes {id = 79 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 178 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 176 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 177 : i64, name = "a", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 175 : i64, op = 0 : i64}
              } attributes {id = 179 : i64, op = 2 : i64}
            } attributes {id = 174 : i64}
          } attributes {id = 173 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<0, 0, 1048576, -1>}
      } attributes {id = 88 : i64}
    } attributes {caches = {_37 = #iir.cache<accessID: 37, type: "K", policy: "Flush", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0], window: [0, 0]>, _38 = #iir.cache<accessID: 38, type: "K", policy: "Fill", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>}, id = 90 : i64, loopOrder = 0 : i64}
  } attributes {id = 41 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_41"}
    } attributes {id = 42 : i64}
  } attributes {id = 41 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_41"}
  } attributes {id = 42 : i64}
} attributes {accessIDToName = {_37 = "d", _38 = "a", _39 = "b", _40 = "c"}, accessIDType = {_37 = 6 : i64, _38 = 6 : i64, _39 = 6 : i64, _40 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [37, 38, 39, 40], fieldAccessIDs = [37, 38, 39, 40], fieldIDToLegalDimensions = {_37 = [1, 1, 1], _38 = [1, 1, 1], _39 = [1, 1, 1], _40 = [1, 1, 1]}, filename = "../../kparallel_solver.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__81 = "2.1000000000000001", __85 = "2", __86 = "1"}, stencilName = "kparallel_solver", temporaryFieldIDs = [], variableVersions = {}}
