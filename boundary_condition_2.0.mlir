iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 58 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 57 : i64, name = "out", negateOffset = false, offset = [0, 1, 0]}
              } attributes {id = 59 : i64, op = 0 : i64}
            } attributes {id = 56 : i64}
          } attributes {id = 55 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 50 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 65 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 63 : i64, name = "in", negateOffset = false, offset = [0, -1, 0]}
                } {
                  iir.var_access_expr {
                  } attributes {id = 64 : i64, isExternal = true, name = "in_glob"}
                } attributes {id = 62 : i64, op = 0 : i64}
              } attributes {id = 66 : i64, op = 0 : i64}
            } attributes {id = 61 : i64}
          } attributes {id = 60 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 52 : i64}
    } attributes {caches = {}, id = 54 : i64, loopOrder = 0 : i64}
  } attributes {id = 30 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_30"}
    } attributes {id = 31 : i64}
  } attributes {id = 30 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_30"}
  } attributes {id = 31 : i64}
} attributes {accessIDToName = {_26 = "in_glob", _27 = "in", _28 = "out", _29 = "bcfield", _49 = "out_0"}, accessIDType = {_26 = 0 : i64, _27 = 6 : i64, _28 = 6 : i64, _29 = 6 : i64, _49 = 4 : i64}, allocatedFieldIDs = [49], apiFieldIDs = [27, 28, 29], fieldAccessIDs = [27, 28, 29, 49], fieldIDToLegalDimensions = {_27 = [1, 1, 1], _28 = [1, 1, 1], _29 = [1, 1, 1]}, filename = "../../boundary_condition_2.cpp", globalVariableIDs = [26], globalVariableToValue = {in_glob = {type = 2 : i64, value = 1.200000e+01 : f64, valueIsSet = true}}, literalIDToName = {}, stencilName = "split_stencil", temporaryFieldIDs = [], variableVersions = {_28 = [49]}}
