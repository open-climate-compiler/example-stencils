"iir.iir"() ( {
  "iir.stencil"() ( {
    "iir.multi_stage"() ( {
      "iir.stage"() ( {
        "iir.do_method"() ( {
          "iir.block_statement"() ( {
            "iir.var_decl_statement"() ( {
              "iir.field_access_expr"() {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 332 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]} : () -> ()
              "iir._var_decl_statement_end"() : () -> ()
            }) {dimension = 0 : i64, id = 331 : i64, name = "__local_layer_1_ret_327", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}} : () -> ()
            "iir.var_decl_statement"() ( {
              "iir.var_access_expr"() ( {
                "iir._var_access_expr_end"() : () -> ()
              }) {id = 334 : i64, isExternal = false, name = "__local_layer_1_ret_327"} : () -> ()
              "iir._var_decl_statement_end"() : () -> ()
            }) {dimension = 0 : i64, id = 333 : i64, name = "__local_layer_2_ret_324", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}} : () -> ()
            "iir.var_decl_statement"() ( {
              "iir.var_access_expr"() ( {
                "iir._var_access_expr_end"() : () -> ()
              }) {id = 336 : i64, isExternal = false, name = "__local_layer_2_ret_324"} : () -> ()
              "iir._var_decl_statement_end"() : () -> ()
            }) {dimension = 0 : i64, id = 335 : i64, name = "__local_layer_3_ret_321", op = 0 : i64, type = {builtinType = 4 : i64, is_const = true, is_volatile = false}} : () -> ()
            "iir.expr_statement"() ( {
              "iir.assignment_expr"() ( {
                "iir.field_access_expr"() {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 339 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]} : () -> ()
                "iir._assignment_expr_end"() : () -> ()
              },  {
                "iir.var_access_expr"() ( {
                  "iir._var_access_expr_end"() : () -> ()
                }) {id = 338 : i64, isExternal = false, name = "__local_layer_3_ret_321"} : () -> ()
                "iir._assignment_expr_end"() : () -> ()
              }) {id = 340 : i64, op = 0 : i64} : () -> ()
              "iir._expr_statement_end"() : () -> ()
            }) {id = 337 : i64} : () -> ()
            "iir._block_statement_end"() : () -> ()
          }) {id = 330 : i64} : () -> ()
          "iir._do_method_end"() : () -> ()
        }) {id = 8 : i64, interval = {lowerLevel = 0 : i64, lowerOffset = 0 : i64, upperLevel = 1048576 : i64, upperOffset = 0 : i64}} : () -> ()
        "iir._stage_end"() : () -> ()
      }) {id = 219 : i64} : () -> ()
      "iir._multi_stage_end"() : () -> ()
    }) {caches = {}, id = 320 : i64, loopOrder = 0 : i64} : () -> ()
    "iir._stencil_end"() : () -> ()
  }) {id = 208 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
},  {
  "iir.id_to_stencil_call_entry"() ( {
    "iir.stencil_call_decl_statement"() ( {
      "iir.stencil_call"() {arguments = [], callee = "__code_gen_208"} : () -> ()
      "iir._stencil_call_decl_statement_end"() : () -> ()
    }) {id = 209 : i64} : () -> ()
    "iir._id_to_stencil_call_entry_end"() : () -> ()
  }) {id = 208 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
},  {
  "iir.stencil_call_decl_statement"() ( {
    "iir.stencil_call"() {arguments = [], callee = "__code_gen_208"} : () -> ()
    "iir._stencil_call_decl_statement_end"() : () -> ()
  }) {id = 209 : i64} : () -> ()
  "iir._iir_end"() : () -> ()
}) {accessIDToName = {_206 = "in", _207 = "out", _321 = "__local_layer_3_ret_321", _324 = "__local_layer_2_ret_324", _327 = "__local_layer_1_ret_327"}, accessIDType = {_206 = 6 : i64, _207 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [206, 207], fieldAccessIDs = [206, 207], fieldIDtoLegalDimensions = {_206 = [1, 1, 1], _207 = [1, 1, 1]}, filename = "stencil_functions.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "test_06_stencil", temporaryFieldIDs = [], variableVersions = {}} : () -> ()
