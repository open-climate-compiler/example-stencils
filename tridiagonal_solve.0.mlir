iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 213 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 211 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 212 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 210 : i64, op = 3 : i64}
              } attributes {id = 214 : i64, op = 0 : i64}
            } attributes {id = 209 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 219 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 217 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 218 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
                } attributes {id = 216 : i64, op = 3 : i64}
              } attributes {id = 220 : i64, op = 0 : i64}
            } attributes {id = 215 : i64}
          } attributes {id = 208 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 0, 0>}
        iir.do_method {
          iir.block_statement {
            iir.var_decl_statement {
              iir.binary_operator {
                iir.literal_access_expr {builtinType = 4 : i64, id = 224 : i64, value = "1"}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 226 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 228 : i64, name = "a", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 229 : i64, name = "c", negateOffset = false, offset = [0, 0, -1]}
                  } attributes {id = 227 : i64, op = 2 : i64}
                } attributes {id = 225 : i64, op = 1 : i64}
              } attributes {id = 223 : i64, op = 3 : i64}
            } attributes {dimension = 0 : i64, id = 222 : i64, name = "m", op = 0 : i64, type = {builtinType = 4 : i64, is_const = false, is_volatile = false}}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 234 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 232 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.var_access_expr {
                  } attributes {id = 233 : i64, isExternal = false, name = "m"}
                } attributes {id = 231 : i64, op = 2 : i64}
              } attributes {id = 235 : i64, op = 0 : i64}
            } attributes {id = 230 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 244 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 239 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 241 : i64, name = "a", negateOffset = false, offset = [0, 0, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 242 : i64, name = "d", negateOffset = false, offset = [0, 0, -1]}
                    } attributes {id = 240 : i64, op = 2 : i64}
                  } attributes {id = 238 : i64, op = 1 : i64}
                } {
                  iir.var_access_expr {
                  } attributes {id = 243 : i64, isExternal = false, name = "m"}
                } attributes {id = 237 : i64, op = 2 : i64}
              } attributes {id = 245 : i64, op = 0 : i64}
            } attributes {id = 236 : i64}
          } attributes {id = 221 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 1, 1048576, 0>}
      } attributes {id = 107 : i64}
    } attributes {caches = {_51 = #iir.cache<accessID: 51, type: "K", policy: "FillFlush", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>, _54 = #iir.cache<accessID: 54, type: "K", policy: "FillFlush", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>}, id = 117 : i64, loopOrder = 1 : i64}
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 251 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 249 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 250 : i64, name = "d", negateOffset = false, offset = [0, 0, 1]}
                } attributes {id = 248 : i64, op = 2 : i64}
              } attributes {id = 252 : i64, op = 2 : i64}
            } attributes {id = 247 : i64}
          } attributes {id = 246 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<0, 0, 1048576, -1>}
      } attributes {id = 115 : i64}
    } attributes {caches = {_51 = #iir.cache<accessID: 51, type: "K", policy: "FillFlush", interval: [0, 0, 1048576, -1], enclosingAccessInterval: [0, 0, 1048576, 0]>}, id = 118 : i64, loopOrder = 2 : i64}
  } attributes {id = 55 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_55"}
    } attributes {id = 56 : i64}
  } attributes {id = 55 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_55"}
  } attributes {id = 56 : i64}
} attributes {accessIDToName = {_112 = "__local_m_112", _51 = "d", _52 = "a", _53 = "b", _54 = "c"}, accessIDType = {_51 = 6 : i64, _52 = 6 : i64, _53 = 6 : i64, _54 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [51, 52, 53, 54], fieldAccessIDs = [51, 52, 53, 54], fieldIDToLegalDimensions = {_51 = [1, 1, 1], _52 = [1, 1, 1], _53 = [1, 1, 1], _54 = [1, 1, 1]}, filename = "../../tridiagonal_solve.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__113 = "1"}, stencilName = "tridiagonal_solve", temporaryFieldIDs = [], variableVersions = {}}
