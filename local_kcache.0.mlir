iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 957 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 956 : i64, value = "4.0999999999999996"}
              } attributes {id = 958 : i64, op = 0 : i64}
            } attributes {id = 955 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 961 : i64, name = "out_b", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 960 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 962 : i64, op = 0 : i64}
            } attributes {id = 959 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 965 : i64, name = "out_a", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 964 : i64, name = "a", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 966 : i64, op = 0 : i64}
            } attributes {id = 963 : i64}
          } attributes {id = 954 : i64}
        } attributes {id = 4 : i64, interval = #iir.interval<1048576, -1, 1048576, -1>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 970 : i64, name = "a", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 969 : i64, value = "2.1000000000000001"}
              } attributes {id = 971 : i64, op = 0 : i64}
            } attributes {id = 968 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 974 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 973 : i64, value = "3.1000000000000001"}
              } attributes {id = 975 : i64, op = 0 : i64}
            } attributes {id = 972 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 978 : i64, name = "out_a", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 977 : i64, name = "a", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 979 : i64, op = 0 : i64}
            } attributes {id = 976 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 982 : i64, name = "out_b", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 981 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 983 : i64, op = 0 : i64}
            } attributes {id = 980 : i64}
          } attributes {id = 967 : i64}
        } attributes {id = 3 : i64, interval = #iir.interval<1048576, 0, 1048576, 0>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 987 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 986 : i64, value = "4.0999999999999996"}
              } attributes {id = 988 : i64, op = 0 : i64}
            } attributes {id = 985 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 991 : i64, name = "out_d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 990 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 992 : i64, op = 0 : i64}
            } attributes {id = 989 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 995 : i64, name = "out_c", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 994 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 996 : i64, op = 0 : i64}
            } attributes {id = 993 : i64}
          } attributes {id = 984 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 1, 0, 1>}
      } attributes {id = 267 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1000 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 999 : i64, value = "2.1000000000000001"}
              } attributes {id = 1001 : i64, op = 0 : i64}
            } attributes {id = 998 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1004 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 4 : i64, id = 1003 : i64, value = "3.1000000000000001"}
              } attributes {id = 1005 : i64, op = 0 : i64}
            } attributes {id = 1002 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1008 : i64, name = "out_c", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1007 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 1009 : i64, op = 0 : i64}
            } attributes {id = 1006 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1012 : i64, name = "out_d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1011 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 1013 : i64, op = 0 : i64}
            } attributes {id = 1010 : i64}
          } attributes {id = 997 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 0, 0>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1019 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1017 : i64, name = "c", negateOffset = false, offset = [0, 0, -1]}
                } {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 1018 : i64, value = "1.1000000000000001"}
                } attributes {id = 1016 : i64, op = 2 : i64}
              } attributes {id = 1020 : i64, op = 0 : i64}
            } attributes {id = 1015 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1029 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1024 : i64, name = "d", negateOffset = false, offset = [0, 0, -1]}
                  } {
                    iir.literal_access_expr {builtinType = 4 : i64, id = 1025 : i64, value = "1.1000000000000001"}
                  } attributes {id = 1023 : i64, op = 2 : i64}
                } {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1027 : i64, name = "d", negateOffset = false, offset = [0, 0, -2]}
                  } {
                    iir.literal_access_expr {builtinType = 4 : i64, id = 1028 : i64, value = "1.2"}
                  } attributes {id = 1026 : i64, op = 2 : i64}
                } attributes {id = 1022 : i64, op = 0 : i64}
              } attributes {id = 1030 : i64, op = 0 : i64}
            } attributes {id = 1021 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1033 : i64, name = "out_c", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1032 : i64, name = "c", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 1034 : i64, op = 0 : i64}
            } attributes {id = 1031 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1037 : i64, name = "out_d", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1036 : i64, name = "d", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 1038 : i64, op = 0 : i64}
            } attributes {id = 1035 : i64}
          } attributes {id = 1014 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<0, 2, 1048576, 0>}
      } attributes {id = 247 : i64}
    } attributes {caches = {_123 = #iir.cache<accessID: 123, type: "K", policy: "FillFlush", interval: [1048576, -1, 1048576, 0], enclosingAccessInterval: [1048576, -1, 1048576, 0], window: [0, 0]>, _124 = #iir.cache<accessID: 124, type: "K", policy: "EPFlush", interval: [1048576, -1, 1048576, 0], enclosingAccessInterval: [1048576, -1, 1048576, 0], window: [-2, 0]>, _125 = #iir.cache<accessID: 125, type: "K", policy: "Fill", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>, _126 = #iir.cache<accessID: 126, type: "K", policy: "Local", interval: [0, 0, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>}, id = 276 : i64, loopOrder = 1 : i64}
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1044 : i64, name = "a", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1042 : i64, name = "a", negateOffset = false, offset = [0, 0, 1]}
                } {
                  iir.literal_access_expr {builtinType = 4 : i64, id = 1043 : i64, value = "1.1000000000000001"}
                } attributes {id = 1041 : i64, op = 2 : i64}
              } attributes {id = 1045 : i64, op = 0 : i64}
            } attributes {id = 1040 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1054 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1049 : i64, name = "b", negateOffset = false, offset = [0, 0, 1]}
                  } {
                    iir.literal_access_expr {builtinType = 4 : i64, id = 1050 : i64, value = "1.1000000000000001"}
                  } attributes {id = 1048 : i64, op = 2 : i64}
                } {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1052 : i64, name = "b", negateOffset = false, offset = [0, 0, 2]}
                  } {
                    iir.literal_access_expr {builtinType = 4 : i64, id = 1053 : i64, value = "1.2"}
                  } attributes {id = 1051 : i64, op = 2 : i64}
                } attributes {id = 1047 : i64, op = 0 : i64}
              } attributes {id = 1055 : i64, op = 0 : i64}
            } attributes {id = 1046 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1058 : i64, name = "out_a", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1057 : i64, name = "a", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 1059 : i64, op = 0 : i64}
            } attributes {id = 1056 : i64}
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1062 : i64, name = "out_b", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 1061 : i64, name = "b", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 1063 : i64, op = 0 : i64}
            } attributes {id = 1060 : i64}
          } attributes {id = 1039 : i64}
        } attributes {id = 5 : i64, interval = #iir.interval<0, 0, 1048576, -2>}
      } attributes {id = 271 : i64}
    } attributes {caches = {_123 = #iir.cache<accessID: 123, type: "K", policy: "BPFill", interval: [0, 0, 1048576, -2], enclosingAccessInterval: [0, 0, 1048576, -1], window: [0, 1]>, _124 = #iir.cache<accessID: 124, type: "K", policy: "BPFill", interval: [0, 0, 1048576, -2], enclosingAccessInterval: [0, 0, 1048576, 0], window: [0, 2]>}, id = 277 : i64, loopOrder = 2 : i64}
  } attributes {id = 127 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_127"}
    } attributes {id = 128 : i64}
  } attributes {id = 127 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_127"}
  } attributes {id = 128 : i64}
} attributes {accessIDToName = {_119 = "out_a", _120 = "out_b", _121 = "out_c", _122 = "out_d", _123 = "a", _124 = "b", _125 = "c", _126 = "d"}, accessIDType = {_119 = 6 : i64, _120 = 6 : i64, _121 = 6 : i64, _122 = 6 : i64, _123 = 3 : i64, _124 = 3 : i64, _125 = 3 : i64, _126 = 3 : i64}, allocatedFieldIDs = [], apiFieldIDs = [119, 120, 121, 122], fieldAccessIDs = [119, 120, 121, 122, 123, 124, 125, 126], fieldIDToLegalDimensions = {_119 = [1, 1, 1], _120 = [1, 1, 1], _121 = [1, 1, 1], _122 = [1, 1, 1], _123 = [1, 1, 1], _124 = [1, 1, 1], _125 = [1, 1, 1], _126 = [1, 1, 1]}, filename = "../../local_kcache.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__249 = "2.1000000000000001", __250 = "3.1000000000000001", __254 = "4.0999999999999996", __258 = "1.1000000000000001", __259 = "1.1000000000000001", __260 = "1.2", __264 = "2.1000000000000001", __265 = "3.1000000000000001", __269 = "4.0999999999999996", __273 = "1.1000000000000001", __274 = "1.1000000000000001", __275 = "1.2"}, stencilName = "local_kcache", temporaryFieldIDs = [123, 124, 125, 126], variableVersions = {}}
