iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 70 : i64, name = "tmp", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 66 : i64, name = "in", negateOffset = false, offset = [0, -2, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 67 : i64, name = "in", negateOffset = false, offset = [0, 2, 0]}
                    } attributes {id = 65 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 68 : i64, name = "in", negateOffset = false, offset = [-2, 0, 0]}
                  } attributes {id = 64 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 69 : i64, name = "in", negateOffset = false, offset = [2, 0, 0]}
                } attributes {id = 63 : i64, op = 0 : i64}
              } attributes {id = 71 : i64, op = 0 : i64}
            } attributes {id = 62 : i64}
          } attributes {id = 61 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 56 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 81 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 77 : i64, name = "tmp", negateOffset = false, offset = [0, -1, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 78 : i64, name = "tmp", negateOffset = false, offset = [0, 1, 0]}
                    } attributes {id = 76 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 79 : i64, name = "tmp", negateOffset = false, offset = [-1, 0, 0]}
                  } attributes {id = 75 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 80 : i64, name = "tmp", negateOffset = false, offset = [1, 0, 0]}
                } attributes {id = 74 : i64, op = 0 : i64}
              } attributes {id = 82 : i64, op = 0 : i64}
            } attributes {id = 73 : i64}
          } attributes {id = 72 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 58 : i64}
    } attributes {caches = {_27 = #iir.cache<accessID: 27, type: "IJ", policy: "Local">}, id = 60 : i64, loopOrder = 0 : i64}
  } attributes {id = 28 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_28"}
    } attributes {id = 29 : i64}
  } attributes {id = 28 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_28"}
  } attributes {id = 29 : i64}
} attributes {accessIDToName = {_25 = "in", _26 = "out", _27 = "tmp"}, accessIDType = {_25 = 6 : i64, _26 = 6 : i64, _27 = 3 : i64}, allocatedFieldIDs = [], apiFieldIDs = [25, 26], fieldAccessIDs = [25, 26, 27], fieldIDToLegalDimensions = {_25 = [1, 1, 1], _26 = [1, 1, 1], _27 = [1, 1, 1]}, filename = "../../lap.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "lap", temporaryFieldIDs = [27], variableVersions = {}}
