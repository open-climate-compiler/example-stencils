iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 195 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 191 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 192 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                    } attributes {id = 190 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 193 : i64, name = "in", negateOffset = false, offset = [0, 0, 2]}
                  } attributes {id = 189 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 194 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                } attributes {id = 188 : i64, op = 0 : i64}
              } attributes {id = 196 : i64, op = 0 : i64}
            } attributes {id = 187 : i64}
          } attributes {id = 186 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 1, 0, 2>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 210 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.binary_operator {
                        iir.binary_operator {
                          iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 204 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                        } {
                          iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 205 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                        } attributes {id = 203 : i64, op = 0 : i64}
                      } {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 206 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                      } attributes {id = 202 : i64, op = 0 : i64}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 207 : i64, name = "in", negateOffset = false, offset = [0, 0, 2]}
                    } attributes {id = 201 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 208 : i64, name = "out", negateOffset = false, offset = [0, 0, -1]}
                  } attributes {id = 200 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 209 : i64, name = "out", negateOffset = false, offset = [0, 0, -2]}
                } attributes {id = 199 : i64, op = 0 : i64}
              } attributes {id = 211 : i64, op = 0 : i64}
            } attributes {id = 198 : i64}
          } attributes {id = 197 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<0, 3, 1048576, -2>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 225 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.binary_operator {
                      iir.binary_operator {
                        iir.binary_operator {
                          iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 219 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                        } {
                          iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 220 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                        } attributes {id = 218 : i64, op = 0 : i64}
                      } {
                        iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 221 : i64, name = "in", negateOffset = false, offset = [0, 0, 1]}
                      } attributes {id = 217 : i64, op = 0 : i64}
                    } {
                      iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 222 : i64, name = "out", negateOffset = false, offset = [0, 0, -1]}
                    } attributes {id = 216 : i64, op = 0 : i64}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 223 : i64, name = "out", negateOffset = false, offset = [0, 0, -2]}
                  } attributes {id = 215 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 224 : i64, name = "out", negateOffset = false, offset = [0, 0, -3]}
                } attributes {id = 214 : i64, op = 0 : i64}
              } attributes {id = 226 : i64, op = 0 : i64}
            } attributes {id = 213 : i64}
          } attributes {id = 212 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<1048576, -1, 1048576, -1>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 234 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 231 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 232 : i64, name = "in", negateOffset = false, offset = [0, 0, -1]}
                  } attributes {id = 230 : i64, op = 0 : i64}
                } {
                  iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 233 : i64, name = "out", negateOffset = false, offset = [0, 0, -1]}
                } attributes {id = 229 : i64, op = 0 : i64}
              } attributes {id = 235 : i64, op = 0 : i64}
            } attributes {id = 228 : i64}
          } attributes {id = 227 : i64}
        } attributes {id = 3 : i64, interval = #iir.interval<1048576, 0, 1048576, 0>}
      } attributes {id = 117 : i64}
    } attributes {caches = {_57 = #iir.cache<accessID: 57, type: "K", policy: "Fill", interval: [0, 1, 1048576, 0], enclosingAccessInterval: [0, 0, 1048576, 0]>, _58 = #iir.cache<accessID: 58, type: "K", policy: "Flush", interval: [0, 1, 1048576, 0], enclosingAccessInterval: [0, 1, 1048576, 0], window: [0, 0]>}, id = 128 : i64, loopOrder = 1 : i64}
  } attributes {id = 59 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_59"}
    } attributes {id = 60 : i64}
  } attributes {id = 59 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_59"}
  } attributes {id = 60 : i64}
} attributes {accessIDToName = {_57 = "in", _58 = "out"}, accessIDType = {_57 = 6 : i64, _58 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [57, 58], fieldAccessIDs = [57, 58], fieldIDToLegalDimensions = {_57 = [1, 1, 1], _58 = [1, 1, 1]}, filename = "../../kcache_flush.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "kcache_flush", temporaryFieldIDs = [], variableVersions = {}}
