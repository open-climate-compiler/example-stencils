iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 127 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 3 : i64, id = 126 : i64, value = "1"}
              } attributes {id = 128 : i64, op = 0 : i64}
            } attributes {id = 125 : i64}
          } attributes {id = 124 : i64}
        } attributes {id = 2 : i64, interval = #iir.interval<4, -2, 4, -2>}
      } attributes {id = 81 : i64}
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 136 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 133 : i64, name = "out", negateOffset = false, offset = [0, 0, 1]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 134 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                  } attributes {id = 132 : i64, op = 0 : i64}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 135 : i64, value = "3"}
                } attributes {id = 131 : i64, op = 0 : i64}
              } attributes {id = 137 : i64, op = 0 : i64}
            } attributes {id = 130 : i64}
          } attributes {id = 129 : i64}
        } attributes {id = 3 : i64, interval = #iir.interval<0, 0, 4, -3>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 141 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.literal_access_expr {builtinType = 3 : i64, id = 140 : i64, value = "0"}
              } attributes {id = 142 : i64, op = 0 : i64}
            } attributes {id = 139 : i64}
          } attributes {id = 138 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<1048576, -1, 1048576, -1>}
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 150 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.binary_operator {
                  iir.binary_operator {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 147 : i64, name = "out", negateOffset = false, offset = [0, 0, 1]}
                  } {
                    iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 148 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
                  } attributes {id = 146 : i64, op = 0 : i64}
                } {
                  iir.literal_access_expr {builtinType = 3 : i64, id = 149 : i64, value = "3"}
                } attributes {id = 145 : i64, op = 0 : i64}
              } attributes {id = 151 : i64, op = 0 : i64}
            } attributes {id = 144 : i64}
          } attributes {id = 143 : i64}
        } attributes {id = 1 : i64, interval = #iir.interval<4, 0, 1048576, -2>}
      } attributes {id = 85 : i64}
    } attributes {caches = {_36 = #iir.cache<accessID: 36, type: "K", policy: "Flush", interval: [0, 0, 1048576, -1], enclosingAccessInterval: [0, 0, 1048576, -1], window: [0, 0]>}, id = 88 : i64, loopOrder = 2 : i64}
  } attributes {id = 37 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_37"}
    } attributes {id = 38 : i64}
  } attributes {id = 37 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_37"}
  } attributes {id = 38 : i64}
} attributes {accessIDToName = {_35 = "in", _36 = "out"}, accessIDType = {_35 = 6 : i64, _36 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [35, 36], fieldAccessIDs = [35, 36], fieldIDToLegalDimensions = {_35 = [1, 1, 1], _36 = [1, 1, 1]}, filename = "../../intervals03.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {__75 = "0", __79 = "3", __83 = "1", __87 = "3"}, stencilName = "intervals03", temporaryFieldIDs = [], variableVersions = {}}
