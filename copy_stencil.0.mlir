iir.iir {
  iir.stencil {
    iir.multi_stage {
      iir.stage {
        iir.do_method {
          iir.block_statement {
            iir.expr_statement {
              iir.assignment_expr {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 27 : i64, name = "out", negateOffset = false, offset = [0, 0, 0]}
              } {
                iir.field_access_expr {argumentMap = [-1, -1, -1], argumentOffset = [0, 0, 0], id = 26 : i64, name = "in", negateOffset = false, offset = [0, 0, 0]}
              } attributes {id = 28 : i64, op = 0 : i64}
            } attributes {id = 25 : i64}
          } attributes {id = 24 : i64}
        } attributes {id = 0 : i64, interval = #iir.interval<0, 0, 1048576, 0>}
      } attributes {id = 21 : i64}
    } attributes {caches = {}, id = 23 : i64, loopOrder = 0 : i64}
  } attributes {id = 11 : i64}
} {
  iir.id_to_stencil_call_entry {
    iir.stencil_call_decl_statement {
      iir.stencil_call {arguments = [], callee = "__code_gen_11"}
    } attributes {id = 12 : i64}
  } attributes {id = 11 : i64}
} {
  iir.stencil_call_decl_statement {
    iir.stencil_call {arguments = [], callee = "__code_gen_11"}
  } attributes {id = 12 : i64}
} attributes {accessIDToName = {_10 = "out", _9 = "in"}, accessIDType = {_10 = 6 : i64, _9 = 6 : i64}, allocatedFieldIDs = [], apiFieldIDs = [9, 10], fieldAccessIDs = [9, 10], fieldIDToLegalDimensions = {_10 = [1, 1, 1], _9 = [1, 1, 1]}, filename = "../../copy_stencil.cpp", globalVariableIDs = [], globalVariableToValue = {}, literalIDToName = {}, stencilName = "copy_stencil", temporaryFieldIDs = [], variableVersions = {}}
